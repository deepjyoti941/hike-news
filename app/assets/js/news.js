var url = document.getElementsByClassName('cta')[0].href;
window.clickHandler = function(ctx) {
    if (typeof PlatformBridge != "undefined") {
        var title = document.getElementsByTagName('h2')[0].innerHTML;
        if (platformSdk.appData.helperData.env) {
            openNews();
        } else {
            PlatformBridge.openFullPage(title, url);
        }

        var ae = {};
        ae["ek"] = "link_clicked_fwd_card";
        ae["c_id"] = document.body.getAttribute("data-cid");
        ae = JSON.stringify(ae);
        try{
          PlatformBridge.logAnalytics(document.getElementsByTagName("body")[0].getAttribute("data-message-id"),"true","click",JSON.stringify(ae));
        }catch(e){
            PlatformBridge.logAnalytics("true","click",JSON.stringify(ae));

        }
    }
};

function checkConnection(fn, ctx) {
    if (platformSdk.bridgeEnabled) {
        platformSdk.nativeReq({
            fn: 'checkConnection',
            ctx: this,
            data: "",
            success: function(response) {
                if (typeof fn === "function") fn(response);
            }
        });
    } else {
        if (navigator.onLine) {
            if (typeof fn === "function") fn.call(ctx);
        } else {
            platformSdk.events.publish('app/offline');
            platformSdk.events.publish('app/hideSplash');
        }
    }
}

function openNews() {
    if (window.installer) {
        return;
    }
    try {
        PlatformBridge.blockParentBot("false");
    } catch (e) {

    }
    platformSdk.nativeReqT({
        fn: 'openNonMessagingBot',
        ctx: this,
        data: ["+hikenews+", JSON.stringify(platformSdk.appData.helperData.news)],
        success: function(response) {
            if (response == "Failure") {

                checkConnection(function(type) {

                    if (type != 0 && type != -1) {

                        PlatformBridge.showToast('Installing News...');
                        var obj = {
                            "apps": [{
                                "name": "hikenews"
                            }],
                            "msisdn": ""
                        };

                        var serverUrl = "http://qa-content.hike.in/mapps/api/v1/apps/";
                        if (platformSdk.appData.helperData.env == "prod") {
                            serverUrl = "http://mapps.platform.hike.in/mapps/api/v2/apps/";
                        }

                        var data = {
                            url: serverUrl + "install.json",
                            params: obj
                        };
                        data = JSON.stringify(data);


                        platformSdk.nativeReq({
                            fn: 'doPostRequest',
                            ctx: this,
                            data: data,
                            success: function(res) {


                            }
                        });
                        window.installer = 1;
                        setTimeout(timeoutTest, 1000);
                        console.log("in timeout");


                    } else {
                        PlatformBridge.showToast("No Internet Connection.");

                    }



                });
            } else {

            }

        }

    });


}
var timeoutTest = function() {
    platformSdk.nativeReq({
        fn: 'openNonMessagingBot',
        ctx: this,
        data: "+hikenews+",
        success: function(response) {
            if (response == "Failure") {
                setTimeout(timeoutTest, 1000);
                console.log("in timeout");
            } else {
                clearTimeout(timeoutTest);
                console.log("clear timeout");


            }
        }
    });
}
    String.prototype.replaceAll = function(find, replace) {
        var str = this;
        return str.replace(new RegExp(find, 'g'), replace);
    };


platformSdk.ready(function(){
    var h = document.getElementsByTagName("h2")[0].innerHTML;
    document.getElementsByTagName("h2")[0].innerHTML = document.getElementsByTagName("h2")[0].innerText;
    document.getElementsByTagName("h2")[0].innerHTML = document.getElementsByTagName("h2")[0].innerHTML.replaceAll("&amp;#39;","'").replaceAll("&#39;","'");

})