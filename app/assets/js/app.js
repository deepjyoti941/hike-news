function app(){
	this.type = '';			// 0: Card; 1: FullScreen; 2: Pinned;
	this.authToken = '';	
	this.cache = {};
	this.endpoints = {};
	this.sel = {};
	this.mt = {};
};


app.prototype.auth = function(){
	// call android bridge to auth app
};

app.prototype.init = function(type){
	this.type = type;
	this.auth();
};

function Gesture(){
	this.touch = {};
};

Gesture.prototype.bind = function(el){

	var that = this;
	var touchEnd = function(ev){
		ev.preventDefault();

		var swipeable = that.el.getAttribute('data-swipeable');

		if (swipeable === "false") that.touch.swipe = "click";
		if (swipeable === "up" && that.touch.swipe === "down") that.touch.swipe = "click";

		that.callback.call(that.el, that.touch, ev);
		that.reset();
		return false;
	};

	var touchStart = function(ev){
		ev.preventDefault();
		that.touch.ts = new Date().getTime();
		that.touch.xStart = typeof ev.changedTouches[0].pageX == 'undefined' ? parseInt(ev.pageX) : parseInt(ev.changedTouches[0].pageX);
		that.touch.yStart = typeof ev.changedTouches[0].pageY == 'undefined' ? parseInt(ev.pageY) : parseInt(ev.changedTouches[0].pageY);

		that.touch.swipe = 'click';
	};

	var touchMove = function(ev){
		ev.preventDefault();

		var self = that;
		var pageheight = platformSdk.utils.getHeight(self.el);
		var pageX = typeof ev.changedTouches[0].pageX == 'undefined' ? ev.pageX : ev.changedTouches[0].pageX;
		var pageY = typeof ev.changedTouches[0].pageY == 'undefined' ? ev.pageY : ev.changedTouches[0].pageY;
		
		var deltaX = parseInt(pageX) - (self.touch.xStart);
		var deltaY = parseInt(pageY) - (self.touch.yStart);
		
		var posX = deltaX + self.touch.lastPosX;
		var posY = deltaY + self.touch.lastPosY;

		var aposX = Math.abs(deltaX + self.touch.lastPosX);
		var aposY = Math.abs(deltaY + self.touch.lastPosY);

		if (aposY > aposX){
			if (posY < 0) { if (posY < -40) self.touch.swipe = 'up'; }
		    else if (posY > 0) { if (posY > 40) self.touch.swipe = 'down'; }
		} else {
			if (posX > 40) { self.touch.swipe = 'right'; }
			else if (posX < -40) { self.touch.swipe = 'left'; }
		}

		return false;
	};

	this.el.addEventListener('touchstart', touchStart, true);
	this.el.addEventListener('touchmove', touchMove, true);
	this.el.addEventListener('touchend', touchEnd, true);

	var unbind = platformSdk.events.subscribe('app/reset/gestures', function(){
		that.el.removeEventListener('touchstart', touchStart, true);
		that.el.removeEventListener('touchmove', touchMove, true);
		that.el.removeEventListener('touchend', touchEnd, true);
		that.el.setAttribute('data-touchev', 'false');
		unbind.remove();
	});
};

Gesture.prototype.reset = function(){
	var that = this;
	this.touch = {
		swipe: '',
		xStart: 0,
		yStart: 0,
		lastPosX: 0,
		lastPosY: 0
	};
};

Gesture.prototype.init = function(el, fn){
	this.callback = fn;
	this.el = el;
	this.reset();
	this.bind(el);
};
