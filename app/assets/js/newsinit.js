(function(){"use strict";function t(t,e){t&&(this.el=t,this.container=e&&e.container,this.reRender=e&&e.reRender)}function e(e){e.style.display="none",t.r=e.offsetTop,e.style.display=""}function i(t,e){if(t)for(var i=e&&e.display,n=t.parentNode.children,l=a.call(n,t),o=l+1,s=n.length;s>o;o++)n[o].style.display=i}function n(t,e){var i=t.getClientRects(),n=0;return p(i,function(t){n+=l(t.height,e)}),n}function l(t,e){return Math.floor(t/e)}function o(){var t=document.createElement("test"),e={},i={Webkit:["WebkitColumnCount","WebkitColumnGap"],Moz:["MozColumnCount","MozColumnGap"],ms:["msColumnCount","msColumnGap"],"":["columnCount","columnGap"]};for(var n in i)i[n][0]in t.style&&(e.columnCount=i[n][0],e.columnGap=i[n][1],e[n.toLowerCase()]=!0);return e}function s(t){return parseInt(t[y.columnCount],10)||1}function h(t){return parseInt(t[y.columnGap],10)||0}function r(t){var e=parseInt(t.lineHeight,10);if(!e)throw Error(f[0]);return e}function u(t){return[t.offsetWidth,t.offsetHeight]}function p(t,e){for(var i=0,n=t.length;n>i&&!e(t[i]);i++);}var a=Array.prototype.indexOf,c=window.getComputedStyle,d="ellipsis-overflowing-child",m="ellipsis-set",f=["The ellipsis container must have line-height set on it"],y=o();t.prototype.calc=function(){if(!this.el)return this;var t=c(this.el),e=u(this.el);return this.columnHeight=e[1],this.columnCount=s(t),this.columnGap=h(t),this.columnWidth=e[0]/this.columnCount,this.lineHeight=r(t),this.deltaHeight=e[1]%this.lineHeight,this.linesPerColumn=Math.floor(this.columnHeight/this.lineHeight),this.totalLines=this.linesPerColumn*this.columnCount,!this.deltaHeight&&this.columnCount>1&&(this.el.style.height=this.columnHeight+"px"),this.child=this.getOverflowingChild(),this},t.prototype.set=function(){return this.el&&this.child?(this.clampChild(),i(this.child.el,{display:"none"}),this.markContainer(),this):this},t.prototype.unset=function(){return this.el&&this.child?(this.el.style.height="",this.unclampChild(this.child),i(this.child.el,{display:""}),this.unmarkContainer(),this.child=null,this):this},t.prototype.destroy=function(){return this.el=this.child=this.container=null,this},t.prototype.getOverflowingChild=function(){var t=this,e={},i=0;return p(this.el.children,function(n){var l,o,s,h=Math.floor(i/t.linesPerColumn)||0;return i+=l=t.getLineCount(n),i>=t.totalLines?(o=i-t.totalLines,s=l-o,e.el=n,e.clampedLines=s,e.clampedHeight=e.clampedLines*t.lineHeight,e.visibleColumnSpan=t.columnCount-h,e.gutterSpan=e.visibleColumnSpan-1,e.applyTopMargin=t.shouldApplyTopMargin(e),y.webkit&&e.clampedLines>1&&(e.clampedHeight+=e.gutterSpan*t.deltaHeight),e):void 0}),e},t.prototype.getLineCount=function(t){return t.offsetWidth>this.columnWidth?n(t,this.lineHeight):l(t.clientHeight,this.lineHeight)},t.prototype.markContainer=function(){this.container&&(this.container.classList.add(m),this.reRender&&e(this.container))},t.prototype.unmarkContainer=function(){this.container&&(this.container.classList.remove(m),this.reRender&&e(this.container))},t.prototype.shouldApplyTopMargin=function(t){var e=t.el;if(y.webkit&&1!==this.columnCount&&!(3>=this.deltaHeight)&&e.previousElementSibling)return 0===e.offsetTop||e.offsetTop===this.columnHeight},t.prototype.clampChild=function(){var t=this.child;t&&t.el&&(t.el.style.height=t.clampedHeight+"px",y.webkit&&(t.el.style.webkitLineClamp=t.clampedLines,t.el.style.display="-webkit-box",t.el.style.webkitBoxOrient="vertical"),this.shouldHideOverflow()&&(t.el.style.overflow="hidden"),t.applyTopMargin&&(t.el.style.marginTop="2em"),t.el.classList.add(d),y.webkit||(t.el.style.position="relative",t.helper=t.el.appendChild(this.helperElement())))},t.prototype.unclampChild=function(t){t&&t.el&&(t.el.style.display="",t.el.style.height="",t.el.style.webkitLineClamp="",t.el.style.webkitBoxOrient="",t.el.style.marginTop="",t.el.style.overflow="",t.el.classList.remove(d),t.helper&&t.helper.parentNode.removeChild(t.helper))},t.prototype.helperElement=function(){var t,e,i=document.createElement("span"),n=this.child.visibleColumnSpan-1;return i.className="ellipsis-helper",i.style.display="block",i.style.height=this.lineHeight+"px",i.style.width="5em",i.style.position="absolute",i.style.bottom=0,i.style.right=0,y.moz&&n&&(t=-(100*n),e=-(n*this.columnGap),i.style.right=t+"%",i.style.marginRight=e+"px",i.style.marginBottom=this.deltaHeight+"px"),i},t.prototype.shouldHideOverflow=function(){var t=this.columnCount>1;return this.columnHeight<this.lineHeight?!0:!t},"object"==typeof exports?(module.exports=function(e,i){return new t(e,i)},module.exports.Ellipsis=t):"function"==typeof define&&define.amd?define(function(){return t}):window.Ellipsis=t})();

var languageMap = {
	tagmap: {
		"5:2": "english",
		"1:2": "hindi",
		"2:2": "telugu",
		"3:2": "tamil",
		"4:2": "malayalam"
	},
	english: {
		category: 'Category',
		fullStory: 'Full Story',
		lang: 'Language',
		updatePill: ' new stories',
		updatePillSingle: ' new story',
		timeago: {
			min: ' minute ago',
			mins: ' minutes ago',
			hour: ' hour ago',
			hours: ' hours ago',
			day: ' day ago',
			days: ' days ago'
		},
		subscribe: {
			"tag_type": 2,
			"tag_id" : 5
		}
	},
	hindi: {
		category: 'श्रेणी',
		fullStory: 'पूर्ण लेख',
		lang: 'भाषा',
		updatePill: ' नई खबर',
		updatePillSingle: ' नई खबर',
		timeago: {
			min: ' मिनट पहले ',
			mins: ' मिनट पहले ',
			hour: ' घंटे पहले',
			hours: ' घंटे पहले',
			day: ' दिन पहले',
			days: ' दिन पहले'
		},
		subscribe: {
			"tag_type": 2,
			"tag_id" : 1
		}
	},
	tamil: {
		category: 'வகை',
		fullStory: 'வகை',
		lang: 'மொழி',
		subscribe: {
			"tag_type": 2,
			"tag_id" : 3
		}
	},
	telugu: {
		category: 'వర్గం',
		fullStory: 'పూర్తి కథ',
		lang: 'భాష',
		subscribe: {
			"tag_type": 2,
			"tag_id" : 2
		}
	},
	malayalam: {
		category: 'വിഭാഗം',
		fullStory: 'പൂർണ്ണ കഥ',
		lang: 'ഭാഷാ',
		subscribe: {
			"tag_type": 2,
			"tag_id" : 4
		}
	}
};

var newsapp = {
	version: "0.2.6",
	env: "prod",
	xhrready: true,
	xhrObject: {},
	readTimer: "",
	readCount: 0,
	renderInstant: "",
	readInterval: "",
	splashTimer: "",
	retryCount: 5,
	httpPool: {
		1: true,
		2: true,
		3: true
	},
	fwdObject : {
        "ld": {},
        "hd": {},
        "layoutId": "newscard.html",
        "push": "silent",
        "notifText": "News Article",
        "h": 200
	},
	subNotifs: {
		"tag_type": 2,
		"tag_id" : 6
	},
	subBlock: {
		"tag_type": 1,
		"tag_id" : 5
	},
	fireTouch: function(touch, ev){
		var that = this;
		switch(touch.swipe){
			case 'click':
				if (ev.target.classList.contains('icon-fwd') || ev.target.classList.contains('fwd')){
					platformSdk.events.publish('app/action/forwardStory');
				} else if (ev.target.classList.contains('icon-share') || ev.target.classList.contains('share')){
					platformSdk.events.publish('app/action/shareStory');
				} else {
					newsapp.app.sel.content.classList.remove('up');
					if (platformSdk.bridgeEnabled) PlatformBridge.allowBackPress("false");
					ev.target.click();	
				}
			break;
			case 'left':
				var nxt = that.nextElementSibling;

				if (newsapp.app.sel.content.classList.contains('up')) {
					newsapp.app.sel.content.classList.remove('up');
					if (platformSdk.bridgeEnabled) PlatformBridge.allowBackPress("false");
				}

				if (nxt != undefined){
					that.getElementsByTagName('figure').innerHTML = '';
					that.classList.add('awayLeft');	
					setTimeout(function(){
						that.classList.remove('active');
						
						nxt.classList.add('active');
						nxt.classList.add('scale');

						var lang = newsapp.app.cache.currentLang;
						var cache = newsapp.app.cache[lang];
						var category = cache.currentCategory;

						that.classList.remove('scale');
						platformSdk.events.publish('app/analytics/readTime/' + (cache[category].mt.currentItem - 1), newsapp.renderInstant);
						platformSdk.events.publish('item/next', that);
					}, 500);

				}

				touch.swipe = 'none';
			break;
			case 'right':
				var previousLi = that.previousElementSibling;

				if (newsapp.app.sel.content.classList.contains('up')) {
					newsapp.app.sel.content.classList.remove('up');
					if (platformSdk.bridgeEnabled) PlatformBridge.allowBackPress("false");
					return;
				}

				if (newsapp.notifSwipeBack){
					newsapp.notifSwipeBack = false;
					platformSdk.events.publish('app/splash/news/swipeback');
					return false;
				}

				if (previousLi){
					previousLi.classList.remove('away');
					previousLi.classList.add('awayLeft');
					previousLi.offsetTop; 					// this is a forced redraw. do not remove. 
					
					previousLi.classList.add('active');	
					previousLi.classList.add('scale');	
					that.classList.remove('active');

					previousLi.classList.remove('killAnim');
					previousLi.offsetTop;
					previousLi.classList.remove('awayLeft');
					setTimeout(function(){
						if (that.previousElementSibling) that.classList.remove('scale');

						var lang = newsapp.app.cache.currentLang;
						var cache = newsapp.app.cache[lang];
						var category = cache.currentCategory;

						platformSdk.events.publish('app/analytics/readTime/' + (cache[category].mt.currentItem + 1), newsapp.renderInstant);
						platformSdk.events.publish('item/prev', that);
					}, 500);
				}

				touch.swipe = 'none';
			break;
			case 'up':
				var nxt = that.nextElementSibling;

				if (newsapp.app.sel.content.classList.contains('up')) {
					newsapp.app.sel.content.classList.remove('up');
					if (platformSdk.bridgeEnabled) PlatformBridge.allowBackPress("false");
				}

				if (nxt != undefined){
					that.getElementsByTagName('figure').innerHTML = '';
					that.classList.add('away');	
					setTimeout(function(){
						that.classList.remove('active');

						nxt.classList.add('active');
						nxt.classList.add('scale');

						var lang = newsapp.app.cache.currentLang;
						var cache = newsapp.app.cache[lang];
						var category = cache.currentCategory;

						that.classList.remove('scale');
						platformSdk.events.publish('app/analytics/readTime/' + (cache[category].mt.currentItem - 1), newsapp.renderInstant);
						platformSdk.events.publish('item/next', that);
					}, 500);
				}

				touch.swipe = 'none';
				
			break;
			case 'down':
				var previousLi = that.previousElementSibling;

				if (newsapp.app.sel.content.classList.contains('up')) {
					newsapp.app.sel.content.classList.remove('up');
					if (platformSdk.bridgeEnabled) PlatformBridge.allowBackPress("false");
					return;
				}

				if (newsapp.notifSwipeBack){
					newsapp.notifSwipeBack = false;
					platformSdk.events.publish('app/splash/news/swipeback');
					return false;
				}

				if (previousLi){
					previousLi.classList.remove('killAnim');
					previousLi.offsetTop;					// this is a forced redraw. do not remove. 
					previousLi.classList.remove('away');
					previousLi.classList.remove('awayLeft');
					previousLi.classList.add('active');	
					previousLi.classList.add('scale');	
					that.classList.remove('active');

					setTimeout(function(){
						if (that.previousElementSibling) that.classList.remove('scale');
						
						var lang = newsapp.app.cache.currentLang;
						var cache = newsapp.app.cache[lang];
						var category = cache.currentCategory;

						platformSdk.events.publish('app/analytics/readTime/' + (cache[category].mt.currentItem + 1), newsapp.renderInstant);
						platformSdk.events.publish('item/prev', that);
					}, 500);
					
				}

				touch.swipe = 'none';

			break;
		};
	},
	logImageLoading: function(c, n){

		var cache = newsapp.app.cache[newsapp.app.cache.currentLang];
		var idx = cache[cache.currentCategory].mt.currentItem;

		var image1 = document.createElement('img');
		image1.setAttribute('creation', new Date().getTime());
		image1.addEventListener('load', function(){
			var created_at = parseInt(this.getAttribute('creation'));
			var load_time = new Date().getTime() - created_at;
			var item_id = this.parentNode.parentNode.parentNode.id.split('item')[1];

			newsapp.imageLog = newsapp.imageLog || {};
			newsapp.imageLog[item_id] = load_time;
		});
		
		image1.src = cache[cache.currentCategory].items[idx - 1].imageurl;

		var image2 = document.createElement('img');
		image2.setAttribute('creation', new Date().getTime());
		image2.addEventListener('load', function(){
			var created_at = parseInt(this.getAttribute('creation'));
			var load_time = new Date().getTime() - created_at;
			var item_id = this.parentNode.parentNode.parentNode.id.split('item')[1];

			newsapp.imageLog = newsapp.imageLog || {};
			newsapp.imageLog[item_id] = load_time;
		});
		
		image2.src = cache[cache.currentCategory].items[idx].imageurl;

		c.getElementsByClassName('imgContainer')[0].appendChild(image1);
		n.getElementsByClassName('imgContainer')[0].appendChild(image2);

	},
	bindGestures: function(){
		var self = this;
		var app = this.app;
		var currentLi = app.sel.newsList.getElementsByClassName('active')[0];
		var nextLi = currentLi.nextElementSibling;

		if (newsapp.logImageLoad) newsapp.logImageLoading(currentLi, nextLi);
		// var previousLi = currentLi.previousElementSibling;

		var touch = new Gesture();
		touch.init(currentLi, this.fireTouch);

		var splash_touch = new Gesture();
		splash_touch.init(this.app.sel.categorySplash, function(touch, ev){
			var that = this;
			switch(touch.swipe){
				case 'up':
				that.classList.add('away');
				setTimeout(function(){
					platformSdk.events.publish('app/splash/swipeUp');
				}, 320);
				break;

				case 'left':
				that.classList.add('awayLeft');
				setTimeout(function(){
					platformSdk.events.publish('app/splash/swipeUp');
				}, 320);
				break;
			};
		});

		splash_touch.init(this.app.sel.newsSplash, function(touch, ev){
			var that = this;
			switch(touch.swipe){
				case 'click':
					if (ev.target.classList.contains('icon-fwd') || ev.target.classList.contains('fwd')){
						platformSdk.events.publish('app/action/forwardStory');
					} else if (ev.target.classList.contains('icon-share') || ev.target.classList.contains('share')){
						platformSdk.events.publish('app/action/shareStory');
					} else {
						newsapp.app.sel.content.classList.remove('up');
						if (platformSdk.bridgeEnabled) PlatformBridge.allowBackPress("false");
						ev.target.click();	
					}
				break;
				case 'up':
					if (!ev.currentTarget.classList.contains('newsSplash')) return false;
					that.classList.add('away');
					setTimeout(function(){
						platformSdk.events.publish('app/splash/swipeUp');
					}, 320);
				break;

				case 'left':
					if (!ev.currentTarget.classList.contains('newsSplash')) return false;
					that.classList.add('awayLeft');
					setTimeout(function(){
						platformSdk.events.publish('app/splash/swipeUp');
					}, 320);
				break;
			};
		});

		currentLi.setAttribute('data-touchev', 'true');
	},
	imageSynchro: function(src, fn){

	 	var img = new Image,
		    canvas = document.createElement("canvas"),
		    ctx = canvas.getContext("2d");

		img.crossOrigin = "Anonymous";
		img.onload = function() {
		    canvas.width = img.width;
		    canvas.height = img.height;
		    ctx.drawImage( img, 0, 0 );
		    
		    var imgdata = canvas.toDataURL('image/jpg');
		    delete canvas;
		    delete img;

		    
		    if (typeof fn === 'function') fn(imgdata);
		};

		img.src = src;
	},
	kill: function(){
		var data = {
			'type': 'splash',
			'action': 'kill',
			'idx': 0,
			'icon': 'repair',
			'classname': 'repair',
			'heading': 'We will be back soon.',
			'text_primary': 'News is currently trying to improve itself.',
		};	
		newsapp.prepareSplash('splash', data);
		newsapp.app.sel.nativeButtons.classList.add('off');
	},
	timeoutSplash: function(){
		var data = {
			'type': 'splash',
			'action': 'timeout',
			'idx': 0,
			'icon': 'nointernet',
			'classname': 'networkerror',
			'isAway': true,
			'heading': 'Check Internet',
			'text_primary': 'This seems to be taking longer than usual.',
			'text_secondary': 'Would you mind checking your Internet.',
			'btn_primary': 'Retry',
			primaryFn: function(){
				newsapp.fetchDiff();
				newsapp.app.sel.categorySplash.classList.remove('active');
				newsapp.app.sel.nativeButtons.classList.remove('off');

				var ae = {};
				ae["ek"] = "retry_click";
				ae["cat"] = newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory;
				ae["mapp_vs"] = newsapp.version;

				if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
			}
		};

		newsapp.prepareSplash('splash', data);
		newsapp.app.sel.nativeButtons.classList.add('off');
	},
	ftu: function(){
		var data = {
			'type': 'ftu',
			'action': 'ftu',
			'idx': 0,
			'icon': 'mainlogo_v',
			'classname': 'blockNotif',
			'heading': 'Enjoying News?',
			'text_primary': 'Continue to get the latest updates throughout the day.',
			'btn_primary': 'Ok',
			'btn_secondary': 'No Thanks',
			primaryFn: function(ev){
				var ctx = document.getElementsByClassName('blockNotif')[0];
				platformSdk.events.publish('app.item.alphaout', ctx);
			}, 
			secondaryFn: function(ev){
				platformSdk.events.publish('app.blockNotif.choice');
			}
		};	
		newsapp.prepareSplash('ftue', data);
		platformSdk.events.publish('app.ftue.subscribe.render');
	},
	offline: function(){
		var data = {
			'type': 'splash',
			'action': 'offline',
			'idx': 0,
			'icon': 'nointernet',
			'classname': 'offline',
			'heading': 'No Internet access',
			'text_primary': 'Try turning on your WiFi or Mobile Data to continue reading News.',
			'btn_primary': 'RETRY',
			'btn_secondary': 'Read Offline',
			primaryFn: function(ev){
				platformSdk.events.publish('app/category/destroy/offline');
				newsapp.app.sel.nativeButtons.classList.remove('off');
				newsapp.pull(true);

				newsapp.xhrObject.ajaxFinish = platformSdk.events.subscribe('app/ajax/success', function(){
					var lang = newsapp.app.cache.currentLang;
					if (lang != undefined){
						var cache = newsapp.app.cache[lang];
						var topic = cache[cache.currentCategory];
						platformSdk.events.publish('app/draw', topic['mt'].currentItem);
					}

					newsapp.xhrObject.ajaxFinish.remove();	
				});

				var ae = {};
				ae["ek"] = "retry_click";
				ae["cat"] = newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory;
				ae["mapp_vs"] = newsapp.version;

				if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
			},
			secondaryFn: function(){
				newsapp.readOffline = true;
				platformSdk.events.publish('app/category/destroy/offline');
			}
		};	
		newsapp.prepareSplash('splash', data);
	},
	languageIntroSplash: function(){

		if (newsapp.app.hd.languageIntroSplash) return;

		var el = document.getElementsByClassName('languageIntroSplash')[0];
		var ok = el.getElementsByClassName('btn-ok')[0];
		var mockBtn = el.getElementsByClassName('mocknativebutton')[0];

		el.classList.remove('hide');
		setTimeout(function(){ 
			el.classList.add('active');
		}, 100);

		var fn_ok = function(){
			// ev.preventDefault();
			el.classList.remove('active');
			setTimeout(function(){
				el.classList.add('hide');
			}, 400);

			newsapp.app.hd.languageIntroSplash = true;
			newsapp.setState();
			ok.removeEventListener('click', fn_ok, false);

			var ae = {};
			ae["ek"] = "micro_app";
			ae["event"] = "lang_intro_splash_dismiss";
			ae["mapp_vs"] = newsapp.version;

			if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
		};

		ok.addEventListener('click', fn_ok, false);
		mockBtn.addEventListener('click', function(ev){
			fn_ok();
			platformSdk.events.publish('app.notification.showLanguageSelector');
		});

		var ae = {};
		ae["ek"] = "micro_app";
		ae["event"] = "lang_intro_splash_vu";
		ae["mapp_vs"] = newsapp.version;

		if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
	},
	introLanguage: function(fn, ctx, oldLang, newLang){
		var el = document.getElementsByClassName('languageIntro')[0];
		var yes = el.getElementsByClassName('btn-yes')[0];
		var no = el.getElementsByClassName('btn-no')[0];

		yes.addEventListener('click', function(ev){
			ev.preventDefault();

			if (typeof fn === "function") fn.call(ctx);
			el.classList.add('away');

			newsapp.app.hd.languageIntro = newsapp.app.hd.languageIntro || {};
			newsapp.app.hd.languageIntro[newsapp.app.cache.currentLang] = true;
			newsapp.setState();	

			newsapp.subscribeRoutine('block', false, newsapp.app.endpoints.subscribe, languageMap[newLang].subscribe, undefined, function(){
				newsapp.subscribeRoutine('block', false, newsapp.app.endpoints.subscribe, newsapp.subBlock);
			});

			var ae = {};
			ae["ek"] = "micro_app";
			ae["event"] = "lang_check_screen_choice";
			ae["fld1"] = newsapp.app.cache.currentLang;
			ae["fld2"] = "yes";
			ae["mapp_vs"] = newsapp.version;

			if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
		});

		no.addEventListener('click', function(ev){
			el.classList.add('away');

			newsapp.app.cache.currentLang = oldLang;
			document.body.className = "news " + newsapp.app.cache.currentLang;
			newsapp.switchCategory(oldLang);

			newsapp.app.hd.languageIntro = newsapp.app.hd.languageIntro || {};
			newsapp.app.hd.languageIntro[newsapp.app.cache.currentLang] = true;
			newsapp.setState();	

			var ae = {};
			ae["ek"] = "micro_app";
			ae["event"] = "lang_check_screen_choice";
			ae["fld1"] = newsapp.app.cache.currentLang;
			ae["fld2"] = "no";
			ae["mapp_vs"] = newsapp.version;

			if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
		});

		el.classList.remove('away');

		var ae = {};
		ae["ek"] = "micro_app";
		ae["event"] = "lang_check_screen_vu";
		ae["fld1"] = newsapp.app.cache.currentLang;
		ae["mapp_vs"] = newsapp.version;

		if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
	},
	subscribeRecurringUser: function(){

		if (newsapp.app.hd.recurringSubscribe === false) return false;
		if (newsapp.app.hd.recurringSubscribe === undefined){
			newsapp.app.hd.recurringSubscribe = true;
			newsapp.app.hd.recurringCount = 1;
			newsapp.setState();
		} else if (newsapp.app.hd.recurringSubscribe){
			if (newsapp.app.hd.recurringCount) {
				newsapp.app.hd.recurringCount++;
				newsapp.setState();
			}

			if (!newsapp.app.hd.autoSubCount) newsapp.app.hd.autoSubCount = 5;

			if (newsapp.app.hd.recurringCount >= newsapp.app.hd.autoSubCount){
				console.log('condition met for ', newsapp.app.hd.recurringCount);

				newsapp.subscribeRoutine('block', false, newsapp.app.endpoints.subscribe, newsapp.subBlock, undefined, function(){
					if (newsapp.app.cache.currentLang && languageMap[newsapp.app.cache.currentLang].subscribe){
						newsapp.subscribeRoutine('block', false, newsapp.app.endpoints.subscribe, languageMap[newsapp.app.cache.currentLang].subscribe);	
					}
				});

				var ae = {};
				ae["ek"] = "ftue_optin_choice";
				ae["subscribe"] = "yes";
				ae["mapp_vs"] = newsapp.version;
				console.log(ae);

				if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);

				newsapp.app.hd.recurringSubscribe = false;
				newsapp.setState();
			}
		}
	},
	introAuto: function(){
		var ftue = {
			'type': 'splash',
			'idx': 0,
			'action': 'ftue',
			'icon': 'mainlogo_v',
			'classname': 'ftue',
			'swipeicon': 'swipe_black',
			'heading': 'Welcome to News',
			'text_primary': 'Available in <strong>English</strong> and <strong>हिन्दी</strong>',
			'text_secondary': "To move on to a new story, simply swipe up on the current story.",
		};	

		newsapp.prepareSplash('splash', ftue);
		platformSdk.events.publish('app.ftue.intro.render');

		var el = document.getElementsByClassName('ftue')[0];
		var swipe = el.getElementsByClassName('swipe_message')[0];
		var splash_touch = new Gesture();
		
		splash_touch.init(el, function(touch, ev){
			var that = this;
			switch(touch.swipe){
				case 'up':
				that.classList.add('away');
				newsapp.app.hd.ftueIntro = false;
				newsapp.setState();

				platformSdk.events.publish('app/intro/subscribe', { 'type': 'swipeup' });

				setTimeout(function(){
					// el.parentNode.removeChild(el);

					var ae = {};
					ae["ek"] = "ftue_intro_swipe_up";
					ae["mapp_vs"] = newsapp.version;

					if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "swipe", ae);
				}, 400);
				break;

				case 'left':
				that.classList.add('awayLeft');
				newsapp.app.hd.ftueIntro = false;
				newsapp.setState();

				platformSdk.events.publish('app/intro/subscribe', { 'type': 'swipeup' });

				setTimeout(function(){
					// el.parentNode.removeChild(el);

					var ae = {};
					ae["ek"] = "ftue_intro_swipe_up";
					ae["mapp_vs"] = newsapp.version;

					if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "swipe", ae);
					// console.log(ae);
				}, 400);
				break;

				case 'right':
				case 'down':
				case 'click':
				that.classList.add('bounce');
				setTimeout(function(){
					that.classList.remove('bounce');

					var ae = {};
					ae["ek"] = "ftue_intro_wrong_swipe";
					ae["direction"] = touch.swipe;
					ae["mapp_vs"] = newsapp.version;

					if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "swipe", ae);
					// console.log(ae);
				}, 2000);
				break;
			};
		});

		setTimeout(function(){
			swipe.classList.add('bounce');
		}, 1000);

		el.classList.add('swiper');
	},
	intro: function(){

		var introTimer = new Date().getTime();
		var complete = { 'swipeup': false, 'xhr': false }
		var subscribeEv = platformSdk.events.subscribe('app/intro/subscribe', function(obj){

			complete[obj.type] = true;

			if (complete.swipeup && complete.xhr){
				// newsapp.subscribeRoutine('block', false, newsapp.app.endpoints.subscribe, newsapp.subBlock);
				var latency = new Date().getTime() - introTimer;

				var ae = {};
				ae["ek"] = "ftue_intro_story_display";
				ae["latency"] = latency;
				ae["mapp_vs"] = newsapp.version;

				if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "swipe", ae);
				subscribeEv.remove();	
			}
		});

		var ftue = {
			'type': 'splash',
			'idx': 0,
			'action': 'ftue',
			'icon': 'mainlogo_v',
			'classname': 'ftue',
			'swipeicon': 'swipe_black',
			'heading': 'Welcome to News',
			'text_primary': 'Available in <strong>English</strong> and <strong>हिन्दी</strong>',
			'text_secondary': "",
			'btn_primary': 'Awesome',
			'btn_secondary': 'I\'m not interested',
			primaryFn: function(ev){
				var ctx = document.getElementsByClassName('ftue')[0];
				platformSdk.events.publish('app.item.alphaout', ctx);

				var el = document.getElementsByClassName('ftue')[0];
				var swipe = el.getElementsByClassName('swipe_message')[0];
				var splash_touch = new Gesture();
				
				splash_touch.init(el, function(touch, ev){
					var that = this;
					switch(touch.swipe){
						case 'up':
						that.classList.add('away');
						newsapp.app.hd.ftueIntro = false;
						newsapp.setState();

						platformSdk.events.publish('app/intro/subscribe', { 'type': 'swipeup' });

						setTimeout(function(){
							// el.parentNode.removeChild(el);

							var ae = {};
							ae["ek"] = "ftue_intro_swipe_up";
							ae["mapp_vs"] = newsapp.version;

							if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "swipe", ae);
						}, 400);
						break;

						case 'left':
						that.classList.add('awayLeft');
						newsapp.app.hd.ftueIntro = false;
						newsapp.setState();

						platformSdk.events.publish('app/intro/subscribe', { 'type': 'swipeup' });

						setTimeout(function(){
							// el.parentNode.removeChild(el);

							var ae = {};
							ae["ek"] = "ftue_intro_swipe_up";
							ae["mapp_vs"] = newsapp.version;

							if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "swipe", ae);
							// console.log(ae);
						}, 400);
						break;

						case 'right':
						case 'down':
						case 'click':
						that.classList.add('bounce');
						setTimeout(function(){
							that.classList.remove('bounce');

							var ae = {};
							ae["ek"] = "ftue_intro_wrong_swipe";
							ae["direction"] = touch.swipe;
							ae["mapp_vs"] = newsapp.version;

							if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "swipe", ae);
							// console.log(ae);
						}, 2000);
						break;
					};
				});

				setTimeout(function(){
					swipe.classList.add('bounce');
				}, 1000);
			}, 
			secondaryFn: function(ev){
				platformSdk.events.publish('app.blockNotif.choice');
			}
		};	

		newsapp.prepareSplash('splash', ftue);
		platformSdk.events.publish('app.ftue.intro.render');
	},
	checkConnection: function(fn, ctx){
		if (platformSdk.bridgeEnabled){
			platformSdk.nativeReq({
				fn: 'checkConnection',
				ctx: this,
				data: "",
				success: function(response){
					if (typeof fn === "function") fn(response);
				}
			});	
		} else {
			if (navigator.onLine){
				if (typeof fn === "function") fn.call(ctx);
			} else {
				if (newsapp.readOffline === undefined) platformSdk.events.publish('app/offline');
				platformSdk.events.publish('app/hideSplash');
			}
		}
	},
	toggleBlock: function(bool){
		if (bool){
			var data = {
				'type': 'splash',
				'action': 'block',
				'idx': 0,
				'icon': 'block',
				'classname': 'block',
				'heading': 'News is Blocked',
				'text_primary': 'Don\'t stay under a rock, keep yourself updated with the latest News.',
				'btn_primary': 'Unblock',
				primaryFn: function(ev){
					platformSdk.events.publish('app.menu.om.block');
				}
			};
			newsapp.prepareSplash('block', data);
		} else {
			platformSdk.events.publish('app/category/destroy/block');
		}
	},
	switchCategory: function(cat, ev){
		platformSdk.events.publish('app/category/destroy');
		
		if (!platformSdk.utils.isEmpty(newsapp.xhrObject.ajaxFinish) && typeof newsapp.xhrObject.ajaxFinish.remove === "function" )
			newsapp.xhrObject.ajaxFinish.remove();
		if (!platformSdk.utils.isEmpty(newsapp.xhrObject.Wait) && typeof newsapp.xhrObject.Wait.remove === "function")
			newsapp.xhrObject.Wait.remove();
		
		newsapp.xhrObject[newsapp.app.cache.currentCategory] = {};
		newsapp.tempArray = [];
		newsapp.bootstrapped = false;
		
		newsapp.app.sel.toast.style.display = 'none';
		newsapp.app.sel.toast.style.opacity = 0;

		console.log('category switched to ' + cat);

		platformSdk.events.publish('app/splash/swipeUp');
		this.fetchDiff();

		if (platformSdk.bridgeEnabled) PlatformBridge.allowBackPress("false");

		var newsSplash = document.getElementsByClassName('newsSplash')[0];
		newsSplash.classList.remove('active');
		newsSplash.classList.add('away');

		// console.log(ev.target);
		// if (ev.target.tagName === "SPAN"){
		// 	ev.target.parentNode.classList.add('tapped');
		// } else ev.target.classList.add('tapped');
	},
	draw: function(res, isInit){

		var t = tmpl('listTemplate', res);
		if (isInit) this.sel.newsList.innerHTML = t;
		else this.sel.newsList.innerHTML += t;
		
		if (isInit){
			var firstLi = this.sel.newsList.getElementsByTagName('li')[0];
			var firstLiImage = firstLi.getElementsByTagName('img')[0];
			firstLi.classList.add('active');
			firstLiImage.addEventListener('load', function(ev){
				News.mt.imagesLoaded = 1;
				newsapp.imageSynchro(firstLi, 4);
			});
			firstLiImage.setAttribute('src', firstLiImage.getAttribute('data-src'));
			
			this.bindEvents();
			this.bindGestures();
		}
	},
	bindToast: function(arr){
		var number = arr.length < 10 ? arr.length : '10+';
		var text = arr.length < 2 ? ('1' + languageMap[newsapp.app.cache.currentLang].updatePillSingle) : (number + languageMap[newsapp.app.cache.currentLang].updatePill);

		var execToast = function(ev){
			var newsSplash = document.getElementsByClassName('newsSplash')[0];
			if (newsSplash.classList.contains('active')){
				newsSplash.classList.remove('active');
			}

			platformSdk.events.publish('app/execDiff');
			newsapp.app.sel.toast.style.opacity = 0;

			setTimeout(function(){
				newsapp.app.sel.toast.style.display = 'none';
			}, 500);
		};

		var elClone = this.app.sel.toast.cloneNode();
		this.app.sel.toast.parentNode.replaceChild(elClone, this.app.sel.toast);
		this.app.sel.toast = document.getElementsByClassName('toast')[0];
		this.app.sel.toast.innerHTML = text;
		this.app.sel.toast.addEventListener('click', execToast, false);

		this.app.sel.toast.style.display = 'block';
		this.app.sel.toast.style.opacity = 1;
	},
	diff: function(arr, rebuild_cache){
		var app = newsapp.app;
		if (arr.length > 0){
			var ajaxtimer = new Date().getTime();
			platformSdk.events.publish('app/ajax/init', {type: 'getDataforIds'});
			platformSdk.ajax({
				url: app.endpoints.fetchId,
				type: 'POST',
				timeout: 30000,
				headers: [['Content-Type', 'application/x-www-form-urlencoded']],
				data: 'items=' + arr.join('&items='),
				success: function(res){
					ajaxtimer = new Date().getTime() - ajaxtimer;
					// console.log('getting new content in a temp array');

					var data = JSON.parse(res);
					var lang = newsapp.app.cache.currentLang;
					var cache = newsapp.app.cache[lang];
					var category = cache.currentCategory;

					if (cache.hashmap === undefined) cache.hashmap = {};
					if (cache.hashmap[category] === undefined) cache.hashmap[category] = {};

					// cache[category]['items'] = platformSdk.utils.merge(cache[category]['items'].concat(data.items), 'id');
					newsapp.tempArray = [];
					newsapp.tempArray = rebuild_cache.concat(data.items);
					platformSdk.utils.sort(newsapp.tempArray, 'published_ts', 'desc');		// sorting the els acc to score
					newsapp.tempArray = newsapp.tempArray.slice(0, 10);						// slicing of elements after the tenth element

					var execDiff = platformSdk.events.subscribe('app/execDiff', function(){
						var lang = newsapp.app.cache.currentLang;
						var cache = newsapp.app.cache[lang];
						var category = cache.currentCategory;
						
						cache[category].items = newsapp.tempArray;
						cache[category].hash = cache[category].newHash;							// replacing the hash with new one
						cache[category].mt.currentItem = 1;
						cache[category].mt.currentItems = 10;
						cache[category].mt.loadThreshold = 2;

						cache.hashmap[category] = {};
						newsapp.tempArray = [];

						var count = data.count = cache[category]['items'].length;
						var now = new Date().getTime();
						
						delete cache[category].newHash;									

						for (var i = 0, len = count; i < len; i++){
							var a = cache[category].items[i];
							a.ts = newsapp.timeago(now - a.published_ts);
							if (cache.hashmap[category][a.id] === undefined){
								// a.ts = newsapp.timeago(now - a.published_ts);
								a.idx = i + 1;
								a.count = count;
								a.topicname = newsapp.app.endpoints.languages[newsapp.app.cache.currentLang][category].nativename;
								a.engtopicname = category;

								cache.hashmap[category][a.id] = 1;
							}
						}

						platformSdk.events.publish('app/draw', 1);
						platformSdk.events.publish('app/hideSplash');

						execDiff.remove();
					});

					platformSdk.events.subscribe('app/category/destroy', function(){
						execDiff.remove();
					});

					newsapp.bindToast(arr);
					newsapp.diffCount = 0;
					platformSdk.events.publish('app/ajax/sendreport', {time: ajaxtimer, status: 'success', type: 'getDataforIds'});
				},
				error: function(r, st){
					ajaxtimer = new Date().getTime() - ajaxtimer;

					if (newsapp.diffCount != undefined) newsapp.diffCount++;
					else newsapp.diffCount = 1;

					newsapp.checkConnection(function(type){
						var lang = newsapp.app.cache.currentLang;
						var category = newsapp.app.cache[lang].currentCategory;
						var cache = newsapp.app.cache[lang][category];

						if (platformSdk.utils.isEmpty(cache)){
							if (newsapp.diffCount > newsapp.retryCount) return false;
							newsapp.diff(arr, rebuild_cache);
							return false;

						} else if (cache && cache.items && cache.items.length > cache.mt.currentItem) {
							// console.log('Diff call retrying');
							
							if (newsapp.diffCount > newsapp.retryCount) return false;
							newsapp.diff(arr, rebuild_cache);
							return false;
						} else {
							if (type === "0" || type === "-1"){
								platformSdk.events.publish('app/offline');
								return;
							}

							// show network error popup screen
							var data = {
								'type': 'splash',
								'action': 'error',
								'idx': 0,
								'icon': 'nointernet',
								'classname': 'networkerror',
								'isAway': true,
								'heading': 'Yikes!',
								'text_primary': 'Problem fetching the latest News.',
								'text_secondary': 'Please check your internet connection.',
								'btn_primary': 'Retry',
								'btn_secondary': 'Read Offline Stories',
								primaryFn: function(){
									newsapp.fetchDiff();
									newsapp.app.sel.categorySplash.classList.remove('active');
									newsapp.app.sel.nativeButtons.classList.remove('off');

									platformSdk.events.publish('app/showSplash');

									var ae = {};
									ae["ek"] = "retry_click";
									ae["cat"] = newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory;
									ae["mapp_vs"] = newsapp.version;

									if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
								},
								secondaryFn: function(){
									newsapp.app.sel.categorySplash.classList.remove('active');
									newsapp.app.sel.nativeButtons.classList.remove('off');

									var ae = {};
									ae["ek"] = "offline_story";
									ae["cat"] = newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory;
									ae["mapp_vs"] = newsapp.version;

									if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
								}
							};

							// var cache = newsapp.app.cache;
							// var category = newsapp.app.cache.currentCategory;

							delete data.secondaryFn;
							delete data.btn_secondary;
							
							// if (category && cache[category] && cache[category].items && cache[category].items.length > 3)

							newsapp.prepareSplash('splash', data);
							newsapp.app.sel.nativeButtons.classList.add('off');

							platformSdk.events.publish('app/hideSplash');
							platformSdk.events.publish('app/ajax/sendreport', {time: ajaxtimer, status: 'error', type: 'getDataforIds'});
						}
					}, this);
				}
			});
		} else {
			platformSdk.events.publish('app/draw', cache[category].mt.currentItem);
		}
	},
	fetchDiff: function(){
		var lang = newsapp.app.cache.currentLang;
		var cache = newsapp.app.cache[lang];
		
		if (typeof cache != "undefined" && cache[cache.currentCategory] && typeof cache[cache.currentCategory].mt != "undefined"){

			var category = cache.currentCategory;
			var url = newsapp.app.endpoints.getIds.replace(/{##}/, category);
			var ex_ids = {};

			cache[category].mt.currentItems = cache[category]['items'].length;
			
			if (cache[category].mt.currentItem > cache[category].mt.currentItems) cache[category].mt.currentItem = cache[category].mt.currentItems;
			if (cache[category].mt.currentItems === 0) {
				cache[category]['mt'] = { currentItem: 1, currentItems: 0, totalItems: 100, currentCategory: '', loadIncrement: 10, imagesLoaded: 3 };
				newsapp.pull();
				return;
			}
			
			cache[category].mt.loadThreshold = cache[category].mt.currentItems - 5;
			platformSdk.events.publish('app/draw', cache[category].mt.currentItem);

			for (var i = 0, len = cache[cache.currentCategory].items.length; i < len; i++){
				var a = cache[cache.currentCategory].items[i];
				ex_ids[a.id] = i;
			}

			var ajaxtimer = new Date().getTime();
			platformSdk.events.publish('app/ajax/init', {type: 'getDiffIds'});
			platformSdk.ajax({
				url: url,
				type: 'POST',
				timeout: 30000,
				headers: [['Content-Type', 'application/x-www-form-urlencoded']],
				data: 's=1&e=10&lang=' + newsapp.app.cache.currentLang,
				success: function(res){
					ajaxtimer = new Date().getTime() - ajaxtimer;
					res = JSON.parse(res);

					if (!res.is_app_live){
						newsapp.kill();
						return false;
					}

					if (res.viewporthash != cache[category].hash){
						console.log('orig hash: ' + cache[category].hash + ' and new hash: ' + res.viewporthash);
						// console.log('fetchDiff Response:', res);

						var ids = res.items;
						var arr = [];
						var rebuild_cache = [];

						for (var i = 0, len = ids.length; i < len; i++){
							if (ex_ids[ids[i].id] === undefined) arr.push(ids[i].id);
							else {
								cache[cache.currentCategory].items[ex_ids[ids[i].id]].score = ids[i].score;
								rebuild_cache.push(cache[cache.currentCategory].items[ex_ids[ids[i].id]]);
							}
						}

						cache[category].newHash = res.viewporthash;
						cache[category].mt.totalItems = parseInt(res.viewportcount);
						if (arr.length > 0) newsapp.diff(arr, rebuild_cache, res.viewporthash);
					} 

					newsapp.fetchDiffCount = 0;
					platformSdk.events.publish('app/ajax/sendreport', {time: ajaxtimer, status: 'success', type: 'getDiffIds'});
				},
				error: function(res){
					console.log("fetchDiff call failed with response", res);
					ajaxtimer = new Date().getTime() - ajaxtimer;

					if (newsapp.fetchDiffCount != undefined) newsapp.fetchDiffCount++;
					else newsapp.fetchDiffCount = 1;

					newsapp.checkConnection(function(type){
						var lang = newsapp.app.cache.currentLang;
						var category = newsapp.app.cache[lang].currentCategory;
						var cache = newsapp.app.cache[lang][category];

						if (platformSdk.utils.isEmpty(cache)){
							
							if (newsapp.fetchDiffCount > newsapp.retryCount) return false;
							newsapp.fetchDiff();
							return false;

						} else if (cache && cache.items && cache.items.length > cache.mt.currentItem) {
							console.log('fetchDiff call retrying');

							if (newsapp.fetchDiffCount > newsapp.retryCount) return false;
							newsapp.fetchDiff();
							return false;

						} else {
							if (type === "0" || type === "-1"){
								platformSdk.events.publish('app/offline');
								return;
							}

							var curr = cache;
							if (curr.items.length != curr.mt.currentItems) {
								curr.mt.currentItems = curr.items.length;
								curr.mt.loadThreshold = curr.mt.currentItems - 5;
							}
							if (curr.mt.currentItem > curr.items.length) curr.mt.currentItem = 1;

							platformSdk.events.publish('app/draw', 1);
							platformSdk.events.publish('app/hideSplash');

							platformSdk.events.publish('app/ajax/sendreport', {time: ajaxtimer, status: 'error', type: 'getDiffIds'});

						}
					}, this);
				}
			});
		} else {
			platformSdk.events.publish('app/showSplash');

			if (newsapp.app.cache.currentLang === undefined) {
				if (platformSdk.locale){
					switch(platformSdk.locale){
						case 'hi':
							newsapp.app.cache.currentLang = "hindi";
							break;
						case 'en':
							newsapp.app.cache.currentLang = "english";
							break;
					};
				} else {
					newsapp.app.cache.currentLang = "english";
				}
			}

			newsapp.app.cache[newsapp.app.cache.currentLang] = newsapp.app.cache[newsapp.app.cache.currentLang] || {};
			newsapp.app.cache[newsapp.app.cache.currentLang].hashmap = newsapp.app.cache[newsapp.app.cache.currentLang].hashmap || {};

			if (newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory === undefined){
				newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory = "topstories";
			}
			
			this.pull();
		}
	},
	prepareSplash: function(type, data){
		
		var cache = newsapp.app.cache;
		var category = cache.currentCategory;

		switch(type){
			case 'splash': 
				var domInstance = this.app.sel.categorySplash;
				var html = tmpl('categoryTemplate', data)

				domInstance.classList.remove('hide');
				domInstance.innerHTML = html;

				var btn_primary = domInstance.getElementsByClassName('primary')[0];
				var btn_secondary = domInstance.getElementsByClassName('secondary')[0];

				if (typeof data.primaryFn === "function") btn_primary.addEventListener('touchend', data.primaryFn, true);
				if (typeof data.secondaryFn === "function") btn_secondary.addEventListener('click', data.secondaryFn, true);

				domInstance.classList.remove('hide');
				domInstance.className = 'categorySplash';
				domInstance.classList.add(data.classname);

				domInstance.classList.add('active');
				if (newsapp.app.sel.content.classList.contains('up')) {
					newsapp.app.sel.content.classList.remove('up');
					if (platformSdk.bridgeEnabled) PlatformBridge.allowBackPress("false");
				}

				break;
			case 'news': 

				if (!data.idx) data.idx = 1;

				var timer = new Date().getTime();

				if (newsapp.currentNotif === undefined) newsapp.currentNotif = data;

				var domInstance = this.app.sel.newsSplash;
				var templateData = tmpl('listTemplate', data);

				domInstance.classList.add('pushNotification');
				domInstance.classList.remove('hide');
				domInstance.innerHTML = '<ul class="listContainer card on newsList">' + templateData + '</ul>';

				var swipeSub = platformSdk.events.subscribe('app/splash/swipeUp', function(){
					if (platformSdk.bridgeEnabled) PlatformBridge.deleteAllNotifData();		// deletes all notifications
					
					var swipeback = platformSdk.events.subscribe('app/splash/news/swipeback', function(){
						if (domInstance != undefined){
							domInstance.classList.remove('hide');
							domInstance.classList.remove('away');
							domInstance.classList.remove('awayLeft');
							domInstance.classList.add('pushNotification');	
						}
						
						swipeback.remove();
					});

					newsapp.notifSwipeBack = true;
					domInstance.classList.remove('pushNotification');
					// swipeSub.remove();

					var readTime = new Date().getTime() - timer;
					var ae = {};
					ae.ek = "art_session";
					ae["session_time"] = readTime;
					ae["mapp_vs"] = newsapp.version;
					ae["cat"] = newsapp.app.cache.currentLang + "_breakingNews";
					ae["c_id"] = data && data.id;
					ae["source"] = data && data.source;

					if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
				});

				var forcedSwipe = platformSdk.events.subscribe('app/splash/forcedSwipe', function(){
					domInstance.classList.add('away');
					newsapp.notifSwipeBack = false;
					setTimeout(function(){
						platformSdk.events.publish('app/splash/swipeUp');
					}, 320);
				});

				domInstance.classList.remove('hide');
				domInstance.className = 'newsSplash';
				domInstance.classList.add(data.classname);
				domInstance.getElementsByTagName('li')[0].classList.remove('away');

				domInstance.classList.add('active');
				if (newsapp.app.sel.content.classList.contains('up')) {
					newsapp.app.sel.content.classList.remove('up');
					if (platformSdk.bridgeEnabled) PlatformBridge.allowBackPress("false");
				}

				break;
			case 'block': 
				var domInstance = this.app.sel.blockSplash;
				var html = tmpl('categoryTemplate', data);

				domInstance.innerHTML = html;

				var btn_primary = domInstance.getElementsByClassName('primary')[0];
				var btn_secondary = domInstance.getElementsByClassName('secondary')[0];

				if (typeof data.primaryFn === "function") btn_primary.addEventListener('click', data.primaryFn, false);
				if (typeof data.secondaryFn === "function") btn_secondary.addEventListener('click', data.secondaryFn, false);

				domInstance.classList.remove('hide');
				domInstance.className = 'blockSplash categorySplash';
				domInstance.classList.add(data.classname);

				domInstance.classList.add('active');
				if (newsapp.app.sel.content.classList.contains('up')) {
					newsapp.app.sel.content.classList.remove('up');
					if (platformSdk.bridgeEnabled) PlatformBridge.allowBackPress("false");
				}

				break;

			case 'ftue':
				var domInstance = this.app.sel.categorySplash;
				domInstance.style.opacity = 0;
				domInstance.className = 'categorySplash';
				domInstance.classList.add('hide');
				domInstance.classList.add(data.classname);

				var html = tmpl('categoryTemplate', data)
				domInstance.innerHTML = html;

				var btn_primary = domInstance.getElementsByClassName('primary')[0];
				var btn_secondary = domInstance.getElementsByClassName('secondary')[0];

				if (typeof data.primaryFn === "function") btn_primary.addEventListener('touchend', data.primaryFn, true);
				if (typeof data.secondaryFn === "function") btn_secondary.addEventListener('touchend', data.secondaryFn, true);

				setTimeout(function(){
					domInstance.classList.remove('hide');
					setTimeout(function(){ 
						domInstance.style.opacity = 1;
					},200);
				}, 200);

				if (newsapp.app.sel.content.classList.contains('up')) {
					newsapp.app.sel.content.classList.remove('up');
					if (platformSdk.bridgeEnabled) PlatformBridge.allowBackPress("false");
				}

				break;
		}

		var destroy = platformSdk.events.subscribe('app/category/destroy/' + data.action, function(){
			domInstance.classList.remove('active');
			domInstance.classList.add('hide');
			destroy.remove();
		});
	},
	timeago: function(ts){
		var set = languageMap[newsapp.app.cache.currentLang].timeago;
		if (ts >= 86400000){
			var a = Math.round(ts/86400000);
			return a === 1 ? a + set.day : a + set.days;
		} else if (ts > 3600000){
			var a = Math.round(ts/3600000);
			return a === 1 ? a + set.hour : a + set.hours;
		} else {
			var a = Math.round(ts/60000);
			return a === 1 ? a + set.min : a + set.mins;
		}
	},
	drawImg: function(id, url, pool){
		newsapp.httpPool[pool] = false;
		var img = document.createElement('img');
		img.src = url;
		img.addEventListener('load', function(){
			newsapp.httpPool[pool] = true;
			var el = document.querySelectorAll('#item' + id + ' .imgContainer')[0];
			if (el != undefined) el.appendChild(img);
		});

		img.addEventListener('error', function(){
			newsapp.httpPool[pool] = true;
		});
	},
	getImage: function(id, url){
		if (newsapp.httpPool[1]) newsapp.drawImg(id, url, 1);
		else if (newsapp.httpPool[2]) newsapp.drawImg(id, url, 2);
		else if (newsapp.httpPool[3]) newsapp.drawImg(id, url, 3);
		else console.log('http pool is busy.');
	},
	xhr: function(url, payload, init){
		if (!newsapp.xhrready) return false;
		newsapp.xhrready = false;

		var ajaxtimer = new Date().getTime();
		platformSdk.events.publish('app/ajax/init', {type: 'getMoreNews'});
		platformSdk.ajax({
			url: url,
			type: 'POST',
			timeout: 30000,
			data: payload,
			headers: [['Content-Type', 'application/x-www-form-urlencoded']],
			success: function(res){

				ajaxtimer = new Date().getTime() - ajaxtimer;
				newsapp.latency(ajaxtimer);

				try {
					var data = JSON.parse(res);	
				} catch(e){
					Bugsnag.notifyException(e, "XHR Pull Incorrect JSON.");
					return false;
				}

				if (!data.is_app_live){
					newsapp.kill();
					return false;
				}

				if (data.itemsreturned <= 0) {
					newsapp.xhrready = true;
					return false;
				}

				console.log("ajax fetch for payload", url, payload, init, data);
				
				var app = newsapp.app;
				var cache = app.cache;
				var lang = cache.currentLang;
				var category = data.topicname;

				if (category === undefined || category === null) category = "topstories";
				if (cache[lang] === undefined) cache[lang] = {};
				if (cache[lang].hashmap === undefined) cache[lang].hashmap = cache[lang].hashmap || {};
				if (cache[lang].hashmap[category] === undefined) cache[lang].hashmap[category] = {};
				
				cache[lang][category] = cache[lang][category] || {};
				cache[lang][category]['items'] = app.cache[lang][category]['items'] || [];
				cache[lang][category]['hash'] = data.viewporthash;

				if (cache[lang][category]['items'].length === 0) var count = data.count = 1;
				else var count = data.count = cache[lang][category]['items'][cache[lang][category]['items'].length - 1].idx + 1;
				var now = new Date().getTime();

				if (init) {
					// if (cache[category].mt === undefined) cache[category]['mt'] = { currentItem: 1, currentItems: 0, totalItems: 0, currentCategory: '', loadIncrement: 10, imagesLoaded: 3 };
					platformSdk.events.publish('app/intro/subscribe', {'type': 'xhr'});
				}
				if (cache[lang][category].mt === undefined) cache[lang][category]['mt'] = { currentItem: 1, currentItems: 0, totalItems: 100, currentCategory: '', loadIncrement: 10, imagesLoaded: 3 };

				var initialItems = cache[lang][category].mt.currentItems;			// these are the number of items before we put new items in.

				for (var i = 0, len = data.items.length; i < len; i++){
					var a = data.items[i];
					
					if (cache[lang]['hashmap'][category][a.id] === undefined){
						a.ts = newsapp.timeago(now - a.published_ts);
						a.idx = count + (i + 1);
						a.count = count;
						a.topicname = newsapp.app.endpoints.languages[data.languageName][category].nativename;
						a.engtopicname = data.topicname;
						a.away = false;
						a.active = false;
						a.imagedata = "";

						cache[lang][category]['items'].push(a);
						cache[lang].hashmap[category][a.id] = 1;
						cache[lang][category].mt.currentItems += 1;
					}					
				}
				
				cache[lang][category].mt.totalItems = parseInt(data.viewportcount);
				cache[lang][category].mt.loadThreshold = cache[lang][category].mt.currentItems - 8;

				if (init && newsapp.app.cache[lang].currentCategory === category && newsapp.bootstrapped != true) platformSdk.events.publish('app/draw', 1);
				if (typeof callback === 'function') callback();

				platformSdk.events.publish('app/hideSplash');
				platformSdk.events.publish('app/ajax/sendreport', {time: ajaxtimer, status: 'success', type: 'getMoreNews'});
				newsapp.xhrready = true;

				if (cache[lang][category].mt.currentItems - initialItems < 5){
					var start = (data.s + 10);
					if (start < cache[lang][category].mt.totalItems){
						var newdata = 's=' + start + '&e=' + (data.e + 10);	
						newsapp.xhr(app.endpoints.languages[lang][category].url, newdata, init);
					} else {
						cache[lang][category].mt.totalItems = cache[lang][category].items.length;
					}
				}

				newsapp.xhrCount = 0;
			},
			error: function(r, st){
				console.log('xhr error occured with response', r);

				ajaxtimer = new Date().getTime() - ajaxtimer;
				newsapp.checkConnection(function(type){
					var lang = newsapp.app.cache.currentLang;
					var cache = newsapp.app.cache[newsapp.app.cache[lang].currentCategory];
					
					if (newsapp.xhrCount != undefined) newsapp.xhrCount++;
					else newsapp.xhrCount = 1;

					if (platformSdk.utils.isEmpty(cache)) {
						newsapp.xhrready = true;

						if (newsapp.xhrCount > newsapp.retryCount) return false;
						newsapp.xhr(url, payload, init);
						if (ajaxtimer > 20000) platformSdk.events.publish('app/ajax/sendreport', {time: ajaxtimer, status: 'error', type: 'getMoreNews'});
						return false;
					} else if (cache && cache.items && cache.items.length > cache.mt.currentItem) {
						newsapp.xhrready = true;

						if (newsapp.xhrCount > newsapp.retryCount) return false;
						newsapp.xhr(url, payload, init);
						if (ajaxtimer > 20000) platformSdk.events.publish('app/ajax/sendreport', {time: ajaxtimer, status: 'error', type: 'getMoreNews'});
						return false;

					} else {

						platformSdk.events.publish('app/hideSplash');

						if (type === "0" || type === "-1"){
							if (newsapp.readOffline === undefined) platformSdk.events.publish('app/offline');
							return;
						}

						// show network error popup screen
						var data = {
							'type': 'splash',
							'action': 'error',
							'idx': 0,
							'icon': 'nointernet',
							'classname': 'networkerror',
							'isAway': true,
							'heading': 'Yikes!',
							'text_primary': 'Problem fetching the latest News.',
							'text_secondary': 'Please check your internet connection.',
							'btn_primary': 'Retry',
							'btn_secondary': 'Read Offline Stories',
							primaryFn: function(){
								newsapp.xhrObject.ajaxFinish = platformSdk.events.subscribe('app/ajax/success', function(){
									var lang = newsapp.app.cache.currentLang;
									if (lang != undefined){
										var cache = newsapp.app.cache[lang];
										var topic = cache[cache.currentCategory];
										platformSdk.events.publish('app/reset/gestures');
										platformSdk.events.publish('app/draw', topic['mt'].currentItem);	
									}
									
									newsapp.xhrObject.ajaxFinish.remove();
								});

								newsapp.xhrready = true;
								newsapp.xhr(url, payload, init);
								newsapp.app.sel.categorySplash.classList.remove('active');
								
								if (newsapp.app.sel.newsList.getElementsByClassName('active')[0] && !newsapp.app.sel.newsList.getElementsByClassName('active')[0].classList.contains('categorySplash'))
									newsapp.app.sel.nativeButtons.classList.remove('off');

								var ae = {};
								ae["ek"] = "retry_click";
								ae["cat"] = newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory;
								ae["mapp_vs"] = newsapp.version;

								if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
							},
							secondaryFn: function(){
								newsapp.app.sel.categorySplash.classList.remove('active');
								newsapp.app.sel.nativeButtons.classList.remove('off');

								var ae = {};
								ae["ek"] = "offline_story";
								ae["cat"] = newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory;
								ae["mapp_vs"] = newsapp.version;

								if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
							}
						};

						var cache = newsapp.app.cache;
						var category = newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory;
						
						delete data.secondaryFn;
						delete data.btn_secondary;

						newsapp.prepareSplash('splash', data);
						newsapp.app.sel.nativeButtons.classList.add('off');

						platformSdk.events.publish('app/ajax/sendreport', {time: ajaxtimer, status: 'error', type: 'getMoreNews'});
					}
				}, this);

				newsapp.xhrready = true;
			}
		});
	},
	pull: function(param, callback){

		var that = this;
		var app = that.app;
		var lang = newsapp.app.cache.currentLang;

		if (!lang || lang === undefined) {
			newsapp.app.cache.currentLang = "english";
			newsapp.app.cache[newsapp.app.cache.currentLang] = newsapp.app.cache[newsapp.app.cache.currentLang] || {};
			lang = newsapp.app.cache.currentLang;
		}

		var cache = newsapp.app.cache[lang];

		if (typeof cache === "undefined"){
			newsapp.app.cache.currentLang = "english";
			newsapp.app.cache["english"] = newsapp.app.cache["english"] || {};
			newsapp.app.cache.english["currentCategory"] = newsapp.app.cache.english["currentCategory"] || "topstories";
		}

		if (typeof cache.currentCategory === "undefined"){
			cache.currentCategory = "topstories";
		}

		var category = cache && cache.currentCategory;
		var init = param === undefined;

		if (!cache && !category){
			newsapp.app.cache[lang] = {};
			if (lang === "english") category = newsapp.app.cache[lang].currentCategory = "topstories";
			else category = newsapp.app.cache[lang].currentCategory = "topstories";
		} else cache = cache[category];

		if (init) {
			var data = 's=1&e=10&lang=' + newsapp.app.cache.currentLang;
			platformSdk.events.publish('app/showSplash');
		} else {
			if (category === undefined) category = "topstories";
			if (cache && cache.mt){
				var endquery = (cache.mt.currentItems + cache.mt.loadIncrement);
				var start = (cache.mt.currentItems + 1);
				var data = 's=' + start + '&e=' + endquery + '&lang=' + newsapp.app.cache.currentLang;
				if (start > cache.mt.totalItems) return;
				if (endquery === cache.mt.currentItems) return;
			} else {
				var data = 's=1&e=10&lang=' + newsapp.app.cache.currentLang;
			}
		}

		var url = newsapp.app.endpoints.languages[lang] && newsapp.app.endpoints.languages[lang][category] && newsapp.app.endpoints.languages[lang][category].url;
		if (url != undefined) this.xhr(url, data, init);
		else {
			platformSdk.events.publish('app/hideSplash');
			newsapp.app.hd.initLang = false;
			newsapp.setState();
		}
	},
	getCurrentEl: function(){
		var cache = newsapp.app.cache;
		var item; 

		if (newsapp.app.sel.newsSplash.classList.contains('active') && !newsapp.app.sel.newsSplash.classList.contains('away') && !newsapp.app.sel.newsSplash.classList.contains('awayLeft')) {
			item = newsapp.currentNotif; 
			item.isBreaking = true;
		} else {
			var lang = cache.currentLang;
			var category = cache[lang].currentCategory;
			var idx = cache[lang][category].mt.currentItem - 1;
			item = cache[lang][category].items[idx]; 
		}

		return item;
	},
	bindEvents: function(){
		var that = this.app;

		var iframeHookFn = function(ev){
			ev.preventDefault();

			that.sel.iframe.iframe.src = this.href;
			that.sel.iframe.classList.add('open');
			that.sel.hamburger.classList.add('backBtn');
		};

		var hamburgerFn = function(ev){
			if (this.classList.contains('backBtn')){
				that.sel.iframe.src = '';
				that.sel.iframe.classList.remove('open');
				this.classList.remove('backBtn');
			} 
		};

		var languageSelectFn = function(ev){
			// delete newsapp.app.cache[newsapp.app.cache.currentCategory].newHash;
			that.sel.content.classList.remove('up');
			if (platformSdk.bridgeEnabled) PlatformBridge.allowBackPress("false");

			var oldLanguage = newsapp.app.cache.currentLang;
			var language = this.getAttribute('data-cat');

			document.body.className = "news " + language;
			
			platformSdk.events.publish('app/category/destroy');
			newsapp.app.sel.toast.style.display = 'none';
			newsapp.app.sel.toast.style.opacity = 0;

			if (language === "english" || (newsapp.app.hd.languageIntro != undefined && newsapp.app.hd.languageIntro[language])){
				
				platformSdk.events.publish('app/reset/gestures');

				newsapp.app.cache.currentLang = language;

				newsapp.initGUI(languageMap[newsapp.app.cache.currentLang]);
				newsapp.renderCategories();
				newsapp.fetchDiff();

				newsapp.subscribeRoutine('block', false, newsapp.app.endpoints.subscribe, languageMap[language].subscribe);

				// analytic call
				var ae = {};
				ae["ek"] = "micro_app";
				ae["event"] = "lang_select";
				ae["fld2"] = language;
				ae["fld1"] = oldLanguage;
				ae["mapp_vs"] = newsapp.version;

				if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
			} else {
				newsapp.introLanguage(function(){
					platformSdk.events.publish('app/reset/gestures');

					newsapp.app.cache.currentLang = language;

					newsapp.initGUI(languageMap[newsapp.app.cache.currentLang]);
					newsapp.renderCategories();
					newsapp.fetchDiff();

					// analytic call
					var ae = {};
					ae["ek"] = "micro_app";
					ae["event"] = "lang_select";
					ae["fld2"] = language;
					ae["fld1"] = oldLanguage;
					ae["mapp_vs"] = newsapp.version;

					if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
				}, this, oldLanguage, language);
			}

			platformSdk.events.publish('app/splash/forcedSwipe');
			this.classList.remove('tapped');
		};

		var categorySelectFn = function(ev){
			platformSdk.events.publish('app/reset/gestures');

			var lang = newsapp.app.cache.currentLang;
			var cache = newsapp.app.cache[lang];
			var oldCat = newsapp.app.cache.currentCategory;
			var cat = cache.currentCategory = this.getAttribute('data-cat');
			newsapp.switchCategory(cat, ev);

			that.sel.content.classList.remove('up');

			var li = this.parentNode.querySelectorAll('li.tapped');
			
			for (var i = 0; i < li.length; i++){
				li[i].classList.remove('tapped');
			}

			this.classList.add('tapped');

			if (platformSdk.bridgeEnabled) PlatformBridge.allowBackPress("false");

			// analytic call
			var ae = {};
			ae["ek"] = "cat_select";
			ae["new_cat"] = newsapp.app.cache.currentLang + "_" + cat;
			ae["old_cat"] = newsapp.app.cache.currentLang + "_" + oldCat;
			ae["mapp_vs"] = newsapp.version;

			if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
		};

		var showCategoryList = function(ev){
			platformSdk.events.publish('app.notification.showCategory');

			// analytic call
			var ae = {};
			ae["ek"] = "cat_click";
			ae["mapp_vs"] = newsapp.version;

			if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
		};

		var showLanguageSelector = function(ev){
			platformSdk.events.publish('app.notification.showLanguageSelector');

			// analytic call
			var ae = {};
			ae["ek"] = "micro_app";
			ae["event"] = "language_click";
			ae["fld1"] = newsapp.app.cache.currentLang;
			ae["mapp_vs"] = newsapp.version;

			if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
		}

		var viewFullStory = function(){
			var el = newsapp.getCurrentEl();

			if (el){
				var url = el.url;

				if (!newsapp.app.hd.ftue && !newsapp.app.hd.ftueIntro){
					newsapp.setState();
					newsapp.setStore(newsapp.app.cache);
				}

				el.title = el.title.replace(/&#39;/g, "'");
				el.title = el.title.replace(/&quot;/g, "\"");

				if (typeof PlatformBridge != "undefined") PlatformBridge.openFullPage(el.title, url);
				else {
					if (this.classList.contains('activated')){
						this.classList.remove('activated');
						that.sel.iframe.src = '';
						that.sel.iframe.classList.remove('open');
						that.sel.hamburger.classList.remove('backBtn');
					} else {
						this.classList.add('activated');
						that.sel.iframe.src = url;
						that.sel.iframe.classList.add('open');
						that.sel.hamburger.classList.add('backBtn');
					}	
				}

				var ae = {};
				ae["ek"] = "full_st";
				ae["c_id"] = el.id;
				ae["source"] = el.source;
				ae["p_ts"] = el.published_ts;
				ae["cat"] = newsapp.app.cache.currentLang + "_" + newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory;
				ae["url"] = el.shorturl;
				ae["mapp_vs"] = newsapp.version;

				if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
			}	
		};

		var shareStory = function(){
			var item = newsapp.getCurrentEl();
			var shareMessage ="Get the latest news only on hike!" ;
			var captionText = "";

			if (platformSdk.bridgeEnabled) {
				PlatformBridge.share(shareMessage, captionText);

				var ae = {};
				ae["ek"] = "micro_app";
				ae["event"] = "share_story";

				ae["fld1"] = item.source;
				ae["fld2"] = newsapp.app.cache.currentLang + "_" + newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory;
				ae["fld5"] = item.id;
				ae["fld6"] = item.published_ts;

				ae["mapp_vs"] = newsapp.version;
				platformSdk.utils.logAnalytics("true", "click", ae);

			} else console.log('bridge is undefined');
		};

		var forwardStory = function(){
			var item = newsapp.getCurrentEl();

			if (item){
				var lang = newsapp.app.cache.currentLang;
				// item.topicname = newsapp.app.cache[lang].currentCategory;
				if (item.isBreaking === undefined || !item.isBreaking)
					item.topicname = newsapp.app.endpoints.languages[newsapp.app.cache.currentLang][newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory].nativename;
				item.title = item.title.replace(/&#39;/g, "'");
				newsapp.fwdObject.ld = item;
				newsapp.fwdObject.notifText = item.title;

				var hm = item.title + ' \n ' + item.shorturl;

				if (platformSdk.bridgeEnabled) {
					PlatformBridge.forwardToChat(JSON.stringify(newsapp.fwdObject), hm);

					var ae = {};
					ae["ek"] = "fwd_button_clicked";
					ae["c_id"] = item.id;
					ae["source"] = item.source;
					ae["p_ts"] = item.published_ts;
					ae["cat"] = newsapp.app.cache.currentLang + "_" + newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory;
					ae["mapp_vs"] = newsapp.version;

					platformSdk.utils.logAnalytics("true", "click", ae);
				} else console.log('bridge is undefined, heres the data anyway.', newsapp.fwdObject);	
			}

			return false;
		};
		
		that.sel.getCategory.addEventListener('touchend', showCategoryList, false);
		that.sel.getLanguage.addEventListener('touchend', showLanguageSelector, false);
		
		that.sel.fullStory.addEventListener('touchend', viewFullStory, false);
		platformSdk.utils.addEventListenerList(that.sel.iframehooks, 'click', iframeHookFn, false);
		
		platformSdk.utils.addEventListenerList(that.sel.categoryItems, 'touchend', categorySelectFn, false);
		platformSdk.utils.addEventListenerList(that.sel.languageItems, 'touchend', languageSelectFn, false);

		platformSdk.utils.addEventListenerList(that.sel.taps, 'touchstart', function(ev){ this.classList.add('tapped')}, false);
		platformSdk.utils.addEventListenerList(that.sel.taps, 'touchend', function(ev){ this.classList.remove('tapped')}, false);

		window.onbeforeunload = function(){
			newsapp.setStore(newsapp.app.cache);
			newsapp.setState();
		};

		document.body.addEventListener('touchstart', function(ev){
			if (ev.target.classList.contains('tapable')){
				ev.target.classList.add('tapped');
			}
		}, false);

		document.body.addEventListener('touchend', function(ev){
			if (ev.target.classList.contains('tapable')){
				ev.target.classList.remove('tapped');
			}
		}, false);

		// pubsub calls

		platformSdk.events.subscribe('item/next/done', function(item){
			switch(item){
				case 4: 
					newsapp.languageIntroSplash();
					break;
			};
		});

		platformSdk.events.subscribe('app/action/forwardStory', function(){
			forwardStory();
		});

		platformSdk.events.subscribe('app/action/shareStory', function(){
			shareStory();
		});

		platformSdk.events.subscribe('app/unbindcategories', function(){
			platformSdk.utils.removeEventListenerList(that.sel.categoryItems, 'touchend', categorySelectFn, false);
		});

		platformSdk.events.subscribe('app/bindcategories', function(){
			platformSdk.utils.addEventListenerList(that.sel.categoryItems, 'touchend', categorySelectFn, false);
			platformSdk.utils.addEventListenerList(that.sel.categoryItems, 'touchstart', function(ev){
				this.classList.add('tapped');
			}, false);
		});

		platformSdk.events.subscribe('app/unbindlanguages', function(){
			platformSdk.utils.removeEventListenerList(that.sel.languageItems, 'touchend', languageSelectFn, false);
		});

		platformSdk.events.subscribe('app/bindlanguages', function(){
			platformSdk.utils.addEventListenerList(that.sel.languageItems, 'touchend', languageSelectFn, false);
			platformSdk.utils.addEventListenerList(that.sel.languageItems, 'touchstart', function(ev){
				this.classList.add('tapped');
			}, false);
		});

		platformSdk.events.subscribe('app/ajax/timeout', function(){
			newsapp.xhrready = true;
			var ae = {};
			ae["ek"] = "xhr_timeout";
			ae["cat"] = newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory;
			ae["mapp_vs"] = newsapp.version;

			if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "ajax", ae);
		});

		// platformSdk.events.subscribe('app/ajax/fail', function(){
		// 	newsapp.xhrready = true;
		// });

		// platformSdk.events.subscribe('app/ajax/success', function(){
		// 	newsapp.xhrready = true;
		// });

		platformSdk.events.subscribe('app/ajax/init', function(data){
			var ae = {};
			ae["ek"] = "fetch_init";
			ae["reqType"] = data.type;
			ae["mapp_vs"] = newsapp.version;

			if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "ajax", ae);
			// console.log(ae);
		});

		platformSdk.events.subscribe('app/ajax/sendreport', function(data){
			var ae = {};
			ae["ek"] = "fetch_story_from_server";
			ae["latency"] = data.time;
			ae["status"] = data.status;
			ae["reqType"] = data.type;
			ae["mapp_vs"] = newsapp.version;

			if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "ajax", ae);
		});

		platformSdk.events.subscribe('app.ftue.subscribe.block', function(){
			platformSdk.events.publish('app.menu.om.block');

			var ae = {};
			ae["ek"] = "ftue_optin_choice";
			ae["subscribe"] = "no";
			ae["mapp_vs"] = newsapp.version;

			if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
		});

		platformSdk.events.subscribe('app.ftue.intro.render', function(){
			var ae = {};
			ae["ek"] = "ftue_intro_vu";
			ae["mapp_vs"] = newsapp.version;

			if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
			// console.log(ae);
		});

		platformSdk.events.subscribe('app.ftue.subscribe.render', function(){
			var ae = {};
			ae["ek"] = "ftue_optin_vu";
			ae["mapp_vs"] = newsapp.version;

			if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
			// console.log(ae);
		});

		platformSdk.events.subscribe('app.ftue.notifications.render', function(){
			var ae = {};
			ae["ek"] = "ftue_notif_vu";
			ae["mapp_vs"] = newsapp.version;

			if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
			// console.log(ae);
		});

		platformSdk.events.subscribe('app/offline', function(){
			newsapp.offline();
			newsapp.xhrready = true;
		});

		platformSdk.events.subscribe('app.notification.showLanguageSelector', function(ctx){
			if (that.sel.content.classList.contains('up') && !that.sel.categoryList.classList.contains('hide')){
				that.sel.content.classList.remove('up');
				setTimeout(function(){
					that.sel.categoryList.classList.add('hide')
					that.sel.languageList.classList.remove('hide')
					that.sel.content.classList.add('up');
				}, 300);
			} else {
				that.sel.categoryList.classList.add('hide');
				that.sel.languageList.classList.remove('hide');
				that.sel.content.classList.toggle('up');
			}
			
			var backPress = platformSdk.events.subscribe('onBackPressed', function(){
				that.sel.content.classList.remove('up');
				PlatformBridge.allowBackPress("false");
				backPress.remove();
			});

			newsapp.app.hd.languageIntroSplash = true;
			newsapp.setState();

			if (newsapp.app.hd.languageIndicator === true){
				newsapp.app.hd.languageIndicator = false;
				newsapp.setState();

				var indicator = newsapp.app.sel.getLanguage.getElementsByClassName('load')[0];
				indicator.parentNode.removeChild(indicator);
			}

			if (typeof PlatformBridge != "undefined"){
				if (that.sel.content.classList.contains('up')) PlatformBridge.allowBackPress("true");
				else PlatformBridge.allowBackPress("false");
			}
		});

		platformSdk.events.subscribe('app.notification.showCategory', function(ctx){
			if (that.sel.content.classList.contains('up') && !that.sel.languageList.classList.contains('hide')){
				that.sel.content.classList.remove('up');
				setTimeout(function(){
					that.sel.categoryList.classList.remove('hide')
					that.sel.languageList.classList.add('hide')
					that.sel.content.classList.add('up');
				}, 300);
			} else {
				that.sel.categoryList.classList.remove('hide');
				that.sel.languageList.classList.add('hide');

				that.sel.content.classList.toggle('up');	
			}
			
			var backPress = platformSdk.events.subscribe('onBackPressed', function(){
				that.sel.content.classList.remove('up');
				PlatformBridge.allowBackPress("false");
				backPress.remove();
			});

			if (newsapp.app.hd.categoryIndicator === true){
				newsapp.app.hd.categoryIndicator = false;
				newsapp.setState();

				var indicator = newsapp.app.sel.getCategory.getElementsByClassName('load')[0];
				indicator.parentNode.removeChild(indicator);
			}

			if (typeof PlatformBridge != "undefined"){
				if (that.sel.content.classList.contains('up')) PlatformBridge.allowBackPress("true");
				else PlatformBridge.allowBackPress("false");
			}
		});

		platformSdk.events.subscribe('app.item.swipeup', function(ctx){
			var touch = { swipe: 'up' };
			var ctx = newsapp.app.sel.newsList.getElementsByClassName('active')[0];
			newsapp.fireTouch.call(ctx, touch);
		});

		platformSdk.events.subscribe('app.blockNotif.choice', function(ctx){
			var ae = {};
			ae["ek"] = "ftue_optin_choice";
			ae["subscribe"] = "no";
			ae["mapp_vs"] = newsapp.version;

			if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
			// console.log(ae);

			platformSdk.events.publish('app.ftue.subscribe.block');
		});

		platformSdk.events.subscribe('app.item.alphaout', function(ctx){
			// ctx.classList.add('away');

			ctx.classList.add('swiper');
			ctx.getElementsByTagName('figcaption')[0].innerHTML = "Congratulations!";
			ctx.getElementsByClassName('text_primary')[0].innerHTML = "You are now ready to read hike News.";
			ctx.getElementsByClassName('text')[0].innerHTML = "To move on to a new story, simply swipe up on the current story.";
			
			newsapp.app.hd.ftue = false;
			newsapp.setState();

			setTimeout(function(){
				newsapp.subscribeRoutine('block', false, newsapp.app.endpoints.subscribe, newsapp.subBlock, true);

				var ae = {};
				ae["ek"] = "ftue_optin_choice";
				ae["subscribe"] = "yes";
				ae["mapp_vs"] = newsapp.version;

				if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
				// console.log(ae);
			}, 400);
		});

		platformSdk.events.subscribe('app.state.block.show', function(){
			newsapp.toggleBlock(true);
		});

		platformSdk.events.subscribe('app.state.block.hide', function(){
			newsapp.toggleBlock(false);
		});

		platformSdk.events.subscribe('app.notification.gototop', function(){
			newsapp.toggleBlock(false);
		});

		platformSdk.events.subscribe('app/ftue/notification/yes', function(ctx){
			var id = platformSdk.retrieveId('app.menu.om.mute');
			platformSdk.events.publish('app.item.swipeup', ctx);
			platformSdk.events.publish('app.menu.om.mute', id);
		});

		platformSdk.events.subscribe('app/ftue/notification/no', function(ctx){
			var id = platformSdk.retrieveId('app.menu.om.mute');
			platformSdk.events.publish('app.item.swipeup', ctx);
			platformSdk.events.publish('app.menu.om.mute', id);
		});

		platformSdk.events.subscribe('app/onbeforeunload', function(){
			newsapp.setStore(newsapp.app.cache);
			newsapp.setState();
			// console.log('storing into cache');
		});

		platformSdk.events.subscribe('app/onresume', function(){
			// console.log('newsapp: resume occured');
		});

		platformSdk.events.subscribe('app/showEmptyNews', function(){
			var data = { 'icon': newsapp.app.currentCategory };
			var t = tmpl('listMock', data);
			newsapp.app.sel.newsList.innerHTML = t;
		});

		platformSdk.events.subscribe('app/showSplash', function(){
			newsapp.app.sel.splash.classList.remove('hide');
			newsapp.app.sel.splash.classList.add('active');
			newsapp.splashTimer = new Date().getTime();
		});

		platformSdk.events.subscribe('app/category/destroy', function(){
			that.sel.fullStory.classList.remove('activated');
			// that.sel.iframe.src = '';
			// that.sel.iframe.classList.remove('open');
		});

		platformSdk.events.subscribe('app/hideSplash', function(){
			var timeout = new Date().getTime() - newsapp.splashTimer;
			if (timeout < 1000){
				setTimeout(function(){
					newsapp.app.sel.splash.classList.remove('active');
					setTimeout(function(){
						newsapp.app.sel.splash.classList.add('hide');
					}, 1000);
				}, 1000 - timeout);
			} else {
				newsapp.app.sel.splash.classList.remove('active');
				setTimeout(function(){
					newsapp.app.sel.splash.classList.add('hide');
				}, 1000);
			}
		});

		platformSdk.events.subscribe('app/draw', function(counter){
			if (newsapp.app && newsapp.app.cache){
				var cache = newsapp.app.cache;
				var lang = newsapp.app.cache.currentLang;
				var category = cache[lang].currentCategory;
				var active_el;

				cache = cache[lang];

				if (cache[category]['items']){
					var t = '';
					newsapp.app.sel.nativeButtons.classList.remove('off');

					if (counter === 1){
						var dataset = cache[category]['items'].slice(counter - 1, counter + 2);
						var dataLength = dataset.length;

						if (dataLength < 3) {
							var finish = {
								'idx': cache[category].mt.currentItem + 1,
								'away': false,
								'active': false,
								'type': 'interstitial',
								'action': 'notification',
								'event_primary': 'app.notification.showCategory',
								'icon': 'genius',
								'classname': 'finishScreen',
								'text_primary': 'You have read all ' + category + ' news.',
								'text_primary': 'Please choose a category.',
								'btn_primary': 'Choose Category'
							};
							dataset.push(finish);
						} 

						if (dataset[2]){
							dataset[2].active = false;	
							dataset[2].away = false;
							dataset[2].killAnim = false;
						}

						if (dataset[1]){
							dataset[1].active = false;
							dataset[1].away = false;
							dataset[1].killAnim = false;
						}

						if (dataset[1]){
							dataset[0].active = true;	
							dataset[0].away = false;
							dataset[0].killAnim = false;

						}
						
						active_el = dataset[0];
					} else {
						var dataset = cache[category]['items'].slice(counter - 2, counter + 1);
						var dataLength = dataset.length;

						if (dataLength === 2) {
							if (cache[category].mt.currentItems < cache[category].mt.totalItems){
								var wait = platformSdk.events.subscribe('app/ajax/success', function(){
									var lang = newsapp.app.cache.currentLang;
									if (lang != undefined){
										var topic = newsapp.app.cache[lang][newsapp.app.cache[lang].currentCategory];
										platformSdk.events.publish('app/draw', topic['mt'].currentItem);	
									}
									
									wait.remove();
								});

								var finish = {
									'idx': cache[category].mt.currentItem + 1,
									'away': false,
									'active': false,
									'type': 'interstitial',
									'action': 'notification',
									'event_primary': 'app.notification.showCategory',
									'icon': 'mainlogo_v',
									'swipeicon': 'swipe_black',
									'classname': 'waitingScreen',
									'heading': 'Want more?',
									'text_primary': 'Please wait while we load more news.',
								};

								dataset.push(finish);
							} else {
								var finish = {
									'idx': cache[category].mt.currentItem + 1,
									'away': false,
									'active': false,
									'type': 'interstitial',
									'action': 'notification',
									'event_primary': 'app.notification.showCategory',
									'icon': 'genius',
									'classname': 'finishScreen',
									'heading': 'Genius',
									'text_primary': 'You have read all ' + category + ' news.',
									'btn_primary': 'Choose Category'
								};
								dataset.push(finish);
							}
							
							// if ((cache[category].mt.totalItems - cache[category].mt.currentItem === 1) || (cache[category].mt.totalItems === cache[category].mt.currentItem) || (cache[category].mt.totalItems < cache[category].mt.currentItem))
							
						} else if (dataLength === 1) {
							newsapp.app.sel.nativeButtons.classList.add('off');
							newsapp.bindGestures();
							return false;
						}

						if (dataset[2]){
							dataset[2].active = false;
							dataset[2].away = false;
							dataset[2].killAnim = false;
						}

						if (dataset[1]){
							dataset[1].active = true;	
							dataset[1].away = false;
							dataset[1].killAnim = false;
							active_el = dataset[1];
						}

						if (dataset[0]){
							dataset[0].active = false;
							dataset[0].away = true;
							dataset[0].killAnim = true;
						}
					}

					for (var i = 0; i < dataset.length; i++){
						if (dataset[i].type === "interstitial"){
							t += tmpl('categoryTemplate', dataset[i]);
							if (i === 0 && counter != 1){

								cache[category]['items'].splice(counter - 2, 1);
								cache[category].mt.currentItem = cache[category].mt.currentItem - 1;
								cache[category].mt.currentItems = cache[category].mt.currentItems - 1;
								cache[category].mt.loadThreshold = cache[category].mt.currentItems - 5;

								platformSdk.events.publish('app/draw', cache[category]['mt'].currentItem);
								newsapp.app.hd.ftue = false;
								newsapp.setState();	
								
								return false;
							} 

							if (dataset[i].active) platformSdk.events.publish(dataset[i].renderEvent);
						} else {
							t += newsapp.logImageLoad ? tmpl('listTemplateDebug', dataset[i]) : tmpl('listTemplate', dataset[i]);
							// newsapp.getImage(dataset[i].id, dataset[i].imageurl);
							newsapp.renderInstant = new Date().getTime();
						}
					}

					that.sel.newsList.innerHTML = t;
					newsapp.bindGestures();

					var activeLi = that.sel.newsList.getElementsByClassName('active')[0];
					if (activeLi.classList.contains('categorySplash')){
						newsapp.app.sel.nativeButtons.classList.add('off');
					} else {
						newsapp.app.sel.nativeButtons.classList.remove('off');

						var ae = {};
						ae["ek"] = "active_story";
						ae["c_id"] = active_el.id;
						ae["source"] = active_el.source;
						ae["p_ts"] = active_el.published_ts;
						ae["cat"] = newsapp.app.cache.currentLang + "_" + newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory;
						ae["mapp_vs"] = newsapp.version;

						if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
						// console.log(ae);

						var getReadTime = platformSdk.events.subscribe('app/analytics/readTime/' + counter, function(renderInstant){
							var readTime = new Date().getTime() - renderInstant;
							ae.ek = "art_session";
							ae["session_time"] = readTime;
							ae["mapp_vs"] = newsapp.version;
							ae["cat"] = newsapp.app.cache.currentLang + "_" + ae.cat;

							delete ae.p_ts;
							delete ae.source;

							if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);

							getReadTime.remove();
						});
						
					}
				}
			}
		});

		var itemnext = platformSdk.events.subscribe('item/next', function(el){
			var lang = newsapp.app.cache.currentLang;
			var cache = newsapp.app.cache[lang];
			var category = cache.currentCategory;

			if (cache[category].mt.currentItem > cache[category].mt.currentItems) return false;

			cache[category]['mt'].currentItem += 1;
			platformSdk.events.publish('app/reset/gestures');

			if (cache[category]['mt'].currentItem != 2) platformSdk.events.publish('app/draw', cache[category]['mt'].currentItem);
			else if (cache[category]['mt'].currentItem === 2 && cache[category].items[0].type === "interstitial"){
				cache[category]['items'].splice(0, 1);
				cache[category].mt.currentItem = cache[category].mt.currentItem - 1;
				cache[category].mt.currentItems = cache[category].mt.currentItems - 1;
				cache[category].mt.loadThreshold = cache[category].mt.currentItems - 5;

				platformSdk.events.publish('app/draw', cache[category]['mt'].currentItem);
				
				newsapp.app.hd.ftueIntro = false;
				newsapp.setState();
			} else {
				newsapp.bindGestures();

				var ae = {};
				var active_el = cache[category].items[1];
				ae["ek"] = "active_story";
				ae["c_id"] = active_el.id;
				ae["source"] = active_el.source;
				ae["p_ts"] = active_el.published_ts;
				ae["cat"] = newsapp.app.cache.currentLang + "_" + newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory;
				ae["mapp_vs"] = newsapp.version;

				if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
			}

			if (cache[category]['mt'].currentItem >= cache[category]['mt'].loadThreshold) platformSdk.events.publish('item/load', el);
			if (newsapp.notifSwipeBack) newsapp.notifSwipeBack = false;
			
			platformSdk.events.publish('item/next/done', ++newsapp.readCount);
			console.log('item/next on ' + category + ' on ' + cache[category]['mt'].currentItem)
		});

		var itemprev = platformSdk.events.subscribe('item/prev', function(el){
			var lang = newsapp.app.cache.currentLang;
			var cache = newsapp.app.cache[lang];
			var category = cache.currentCategory;

			cache[category]['mt'].currentItem = (cache[category]['mt'].currentItem - 1) >= 1 ? (cache[category]['mt'].currentItem - 1) : 1;
			platformSdk.events.publish('app/reset/gestures');
			
			if (cache[category]['mt'].currentItem === 1) newsapp.bindGestures();
			else platformSdk.events.publish('app/draw', cache[category]['mt'].currentItem);

			console.log('item/prev on ' + category + ' on ' + cache[category]['mt'].currentItem)
		});

		var itemload = platformSdk.events.subscribe('item/load', function(el){ newsapp.pull(true) });
	},
	getLatestNews: function(obj){
		var arr = [];
		for (var key in obj){
			arr.push(key);
		}

		arr.sort(function(a, b){return b-a});
		return arr[0];
	},
	initDomSelectors: function(){
		this.sel.content = document.getElementsByClassName('content')[0];
		this.sel.newsList = document.getElementsByClassName('newsList')[0];
		this.sel.hamburger = document.getElementsByClassName('hamburger')[0];
		this.sel.closeContainer = document.getElementsByClassName('closeContainer')[0];
		
		this.sel.categoryList = document.getElementsByClassName('catContainer')[0];
		this.sel.categoryItems = this.sel.categoryList.getElementsByTagName('li');

		this.sel.languageList = document.getElementsByClassName('langContainer')[0];
		this.sel.languageItems = this.sel.languageList.getElementsByTagName('li');
		
		this.sel.categorySplash = document.getElementsByClassName('categorySplash')[0];
		this.sel.blockSplash = document.getElementsByClassName('blockSplash')[0];
		this.sel.newsSplash = document.getElementsByClassName('newsSplash')[0];

		this.sel.tmpl = document.getElementById('listTemplate');

		this.sel.iframe = document.getElementById('sourceFrame');
		this.sel.iframehooks = document.getElementsByClassName('cta');

		this.sel.nativeButtons = document.getElementsByClassName('native-row')[0];
		this.sel.getCategory = this.sel.nativeButtons.getElementsByClassName('getCategories')[0];
		this.sel.getLanguage = this.sel.nativeButtons.getElementsByClassName('getLanguages')[0];
		this.sel.fullStory = this.sel.nativeButtons.getElementsByClassName('getFullStory')[0];
		
		this.sel.forwardStory = this.sel.nativeButtons.getElementsByClassName('icon-fwd')[0];
		
		this.sel.taps = document.getElementsByClassName('tap');

		this.sel.toast = document.getElementsByClassName('toast')[0];
		this.sel.splash = document.getElementsByClassName('splash')[0];
	},
	setStore: function(obj){
		// obj.currentCategory = newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory;
		// if (obj[obj.currentCategory] && obj[obj.currentCategory].mt && obj[obj.currentCategory].mt.currentItem > obj[obj.currentCategory].mt.currentItems) {
			// obj[obj.currentCategory].mt.currentItem = obj[obj.currentCategory].mt.currentItems;
		// }
		try {
			platformSdk.setBlob(obj);
		} catch(e){
			if ('localStorage' in window && window['localStorage'] !== null){
				localStorage['appcache'] = JSON.stringify(obj);
			}
		}
	},
	setState: function(){
		var str = platformSdk.utils.validateStringifyJson(newsapp.app.hd);
		try {
			if (newsapp.clientVersion > "397") platformSdk.updateHelperData(newsapp.app.hd);
			else PlatformBridge.putInCache('appState', str);
		} catch(e){
			if ('localStorage' in window && window['localStorage'] !== null){
				localStorage['appState'] = str;
			}
		}
	},
	getStore: function(fn){
		if (!platformSdk.bridgeEnabled && localStorage['appcache'] != undefined && !platformSdk.utils.isEmpty(localStorage['appcache'])){
			this.app.cache = JSON.parse(localStorage['appcache']);
			if (typeof fn === "function") fn();
		} else if (platformSdk.bridgeEnabled){
			platformSdk.nativeReq({
				fn: 'getLargeDataFromCache',
				ctx: this,
				data: "",
				success: function(response){
					if (response === "") {
						platformSdk.events.publish('app/showSplash');
						newsapp.app.cache = {};
						newsapp.app.cache.hashmap = {};
					} else {
						response = decodeURIComponent(response);
						try {
							response = JSON.parse(response);
							newsapp.app.cache = response;
						} catch(e) {
							platformSdk.events.publish('app/showSplash');
							newsapp.app.cache = {};
							newsapp.app.cache.hashmap = {};
							Bugsnag.notifyException(e, "LargeDataCache Incorrect JSON.");
						}	
					}

					if (typeof fn === "function") fn();
				}
			});
		} else {
			this.app.cache = {};
			newsapp.app.cache.hashmap = {};
			if (typeof fn === "function") fn();
		}
	},
	getStateData: function(fn){
		if (!platformSdk.bridgeEnabled && localStorage['appState'] != undefined && !platformSdk.utils.isEmpty(localStorage['appState'])){
			this.app.hd = JSON.parse(localStorage['appState']);
			if (typeof fn === "function") fn();
		} else if (platformSdk.bridgeEnabled){

			newsapp.clientVersion = platformSdk.appVersion.split('.').join('');
			if (newsapp.clientVersion > "397"){
				try {
					newsapp.app.hd = JSON.parse(platformSdk.helperData);
				} catch (e){
					newsapp.app.hd = {};
					Bugsnag.notifyException(e, "StateData Cache Incorrect JSON.");
				}

				if (newsapp.app.hd.cacheMigration === undefined){
					platformSdk.nativeReq({
						fn: 'getFromCache',
						ctx: this,
						data: "appState",
						success: function(response){
							if (response === "") newsapp.app.hd = {};
							else {
								response = decodeURIComponent(response);
								try {
									var response = JSON.parse(response);
								} catch (e){
									var response = {};
								}
							}

							newsapp.app.hd = platformSdk.utils.extend({}, newsapp.app.hd, response);
							newsapp.app.hd.cacheMigration = true;
							newsapp.setState();
							if (typeof fn === "function") fn();
						}
					});	
				} else {
					if (typeof fn === "function") fn();	
				}
				
			} else {
				platformSdk.nativeReq({
					fn: 'getFromCache',
					ctx: this,
					data: "appState",
					success: function(response){
						if (response === "") newsapp.app.hd = {};
						else {
							response = decodeURIComponent(response);
							try {
								newsapp.app.hd = JSON.parse(response);
							} catch (e){
								newsapp.app.hd = {};
							}
						}
						if (typeof fn === "function") fn();
					}
				});
			}
		} else {
			this.app.hd = {};
			if (typeof fn === "function") fn();
		}
	},
	bootstrap: function(){
		for (var cat in newsapp.app.cache){
			if (cat != "hashmap" && cat != "currentCategory"){
				newsapp.app.cache.hashmap[cat] = {}
				if (newsapp.app.cache[cat].items && newsapp.app.cache[cat].mt){
					for (var i = 0, len = newsapp.app.cache[cat].items.length; i < len; i++){
						var item = newsapp.app.cache[cat].items[i];
						newsapp.app.cache.hashmap[cat][item.id] = 1;
					}	
				}
			}
		}
	},
	subscribeRoutine: function(type, bool, url, obj, init, fn){

		var data = JSON.stringify({
			url: url,
			params: obj
		});

		platformSdk.nativeReq({
			fn: 'doPostRequest',
			ctx: this,
			data: data,
			success: function(res){
				res = decodeURIComponent(res);
				try {
					res = JSON.parse(res);
				} catch(e) {
					Bugsnag.notifyException(e, "doPostRequest Incorrect Response.");
					return false;
				}

				try {
					res.response = JSON.parse(res.response);
				} catch(e){}

				// var subs = res.response.subscriptions;
				// if (subs != undefined){
				// 	var sub_lang = languageMap.tagmap[subs[0]];
				// 	if (sub_lang){
				// 		var tag = languageMap[sub_lang].subscribe;
				// 		newsapp.subscribeRoutine('block', false, newsapp.app.endpoints.subscribe, tag);		
				// 	}
				// } 
				
				if (init != undefined && init === true){
					newsapp.subscribeRoutine('block', false, newsapp.app.endpoints.subscribe, languageMap["english"].subscribe);
				}

				if (fn != undefined && typeof fn === "function") fn();
			}
		});
	},
	initOverflowMenu: function(){
		var omList = [{
			"title": platformSdk.block === "true" ? "Unblock" : "Block",
			"en": "true",
			"eventName": "app.menu.om.block"
		},
		{
			"title": "Notifications",
			"en": "true",
			"eventName": "app.menu.om.mute",
			"is_checked": platformSdk.mute === "true" ? "false" : "true"
		},
		{
			"title": "Show Images",
			"en": "true",
			"eventName": "app.menu.om.imageModeToggle",
			"is_checked": newsapp.app.hd.imageMode === true ? "true" : "false",
		}];

		platformSdk.events.subscribe('app.menu.om.mute', function(id){
			id = "" + platformSdk.retrieveId('app.menu.om.mute');
			if (platformSdk.mute == "true"){
				platformSdk.mute = "false";
				platformSdk.muteChatThread();
				platformSdk.updateOverflowMenu(id, {
					"is_checked": "true"
				});
			} else {
				platformSdk.mute = "true";
				platformSdk.muteChatThread();
				platformSdk.updateOverflowMenu(id, {
					"is_checked": "false"
				});
			}

			// analytic call
			var ae = {};
			ae["ek"] = platformSdk.mute === "true" ? "mute" : "unmute";
			ae["mapp_vs"] = newsapp.version;

			if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
		});

		platformSdk.events.subscribe('app.menu.om.block', function(id){
			id = "" + platformSdk.retrieveId('app.menu.om.block');
			if (platformSdk.block === "true"){
				if (platformSdk.bridgeEnabled) platformSdk.unblockChatThread();
				
				platformSdk.block = "false";
				platformSdk.events.publish('app.state.block.hide');
				platformSdk.updateOverflowMenu(id, {
					"title": "Block"
				});

				var url = newsapp.app.endpoints.subscribe;
				var bool = false;

				if (newsapp.app.cache && newsapp.app.cache[newsapp.app.cache.currentLang] && newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory != undefined){
					if (newsapp.app.sel.newsList.childNodes.length === 0){
						newsapp.app.cache[newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory] = {};
						newsapp.app.cache.hashmap[newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory] = {};
						newsapp.pull();
					}
				} else {
					if (newsapp.app.cache.currentLang === undefined) newsapp.app.cache.currentLang = "english";

					newsapp.app.cache[newsapp.app.cache.currentLang] = {};
					newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory = "topstories";
					newsapp.app.cache[newsapp.app.cache.currentLang][newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory] = newsapp.app.cache[newsapp.app.cache.currentLang][newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory] || {};
					newsapp.app.cache[newsapp.app.cache.currentLang].hashmap = newsapp.app.cache[newsapp.app.cache.currentLang].hashmap || {};
					newsapp.app.cache[newsapp.app.cache.currentLang].hashmap[newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory] = {};

					newsapp.pull();
				}

				newsapp.app.sel.content.classList.remove('hide');
			} else {
				if (platformSdk.bridgeEnabled) platformSdk.blockChatThread();
				
				platformSdk.block = "true";
				platformSdk.events.publish('app.state.block.show');
				platformSdk.updateOverflowMenu(id, {
					"title": "Unblock"
				});

				var url = newsapp.app.endpoints.unsubscribe;
				var bool = true;	
			}

			// analytic call
			var ae = {};
			ae["ek"] = platformSdk.block === "true" ? "block" : "unblock";
			ae["mapp_vs"] = newsapp.version;
			if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);

			// var tag = languageMap[newsapp.app.cache.currentLang].subscribe;
			// if (tag === undefined) tag = languageMap["english"].subscribe;

			// newsapp.subscribeRoutine('block', bool, url, newsapp.subBlock);			
			if (newsapp.app.hd.ftueIntro) newsapp.intro();
		});

		platformSdk.events.subscribe('app.menu.om.imageModeToggle', function(id){
			id = "" + platformSdk.retrieveId('app.menu.om.imageModeToggle');
			if (newsapp.app.hd.imageMode === true){
				var images = newsapp.app.sel.newsList.querySelectorAll('li.listItem .imgContainer');
				for (var i = images.length - 1; i >= 0; i--){
					var li = platformSdk.utils.closest(images[i], 'li');
					if (!li.classList.contains('categorySplash')) images[i].parentNode.removeChild(images[i]);
				}
				newsapp.app.hd.imageMode = false;
				newsapp.app.hd.imageModeConfirm = false;
				platformSdk.updateOverflowMenu(id, {
					"is_checked": "false"
				});
				newsapp.setState();

				if (newsapp.app.sel.newsSplash.classList.contains('active')){
					var el = newsapp.app.sel.newsSplash.getElementsByTagName('li')[0];
					var image = el.querySelectorAll('.imgContainer')[0];
					image.parentNode.removeChild(image);
				}
			} else {
				newsapp.app.hd.imageMode = true;
				newsapp.app.hd.imageModeConfirm = true;
				newsapp.killImages = false;			// overriding the default connection test.
				platformSdk.updateOverflowMenu(id, {
					"is_checked": "true"
				});

				var item = newsapp.getCurrentEl();
				if (newsapp.app.sel.newsSplash.classList.contains('active'))
					var el = newsapp.app.sel.newsSplash.getElementsByTagName('li')[0];
				else var el = newsapp.app.sel.newsList.querySelectorAll('li.active')[0];

				var figure = el.getElementsByTagName('figure')[0];
				figure.innerHTML = '<div class="imgContainer" style="background:url(' + item.imageurl + ') no-repeat center center;"></div>' + figure.innerHTML;
				newsapp.setState();
			}

			// analytic call
			var ae = {};
			ae["ek"] = newsapp.app.hd.imageMode === true ? "tm_en" : "tm_dis";
			ae["mapp_vs"] = newsapp.version;

			if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);

		});

		platformSdk.setOverflowMenu(omList);
	},
	sortKeys: function(ob){
		var keys = [];
		for (var key in ob){
			keys.push(key);
		}	

		return keys.sort();
	},
	renderCategories: function(){
		var lang = this.app.endpoints.languages;
		if (lang != undefined){
			if (newsapp.app.cache.currentLang === undefined) newsapp.app.cache.currentLang = "english";
			var categories = lang[newsapp.app.cache.currentLang];
			var html = '';

			for (var cat in categories){
				html += '<li class="tap noselect ' + categories[cat].name + '" data-cat="' + categories[cat].name + '"><span class="category ' + categories[cat].name + '">' + categories[cat].nativename + '</span></li>';
			}	

			platformSdk.events.publish('app/unbindcategories');

			var ul = '<ul class="listContainer catlist card">' + html + '</ul>';
			newsapp.app.sel.categoryList.innerHTML = ul;
		}

		platformSdk.events.publish('app/bindcategories');
	},
	renderLanguages: function(){
		var lang = this.app.endpoints.languages;
		if (lang != undefined){
			var html = "";
			var ascending_list = newsapp.sortKeys(lang);

			for (var i = 0; i < ascending_list.length; i++){
				var key = ascending_list[i];
				html += '<li class="tap noselect ' + key + '" data-cat="' + key + '"><span class="category ' + key + '">' + key + '</span></li>';
			}

			platformSdk.events.publish('app/unbindlanguages');
			
			var ul = '<ul class="listContainer catlist card">' + html + '</ul><span class="langMessage">More languages coming soon.</span>';
			newsapp.app.sel.languageList.innerHTML = ul;
		}

		platformSdk.events.publish('app/bindlanguages');
	},
	checkCategoryUpdate: function(){
		platformSdk.ajax({
			url: newsapp.domain + '/content/v1/channel/news/topics',
			type: 'POST',
			success: function(res){
				try { res = JSON.parse(res); }
				catch(e) { 
					Bugsnag.notifyException(e, "checkCategoryUpdate Incorrect JSON.");
					console.log('invalid response form get topics call.') 
				}

				if (res.viewporthash != newsapp.app.hd.topicsHash){
					newsapp.app.hd.initLang = false;
					if (platformSdk.helperData) delete platformSdk.helperData.topics;
					newsapp.initLanguages(newsapp.domain);
				}
			}
		});
	},
	getLanguages: function(fn){
		if (!platformSdk.bridgeEnabled && localStorage['language'] != undefined && !platformSdk.utils.isEmpty(localStorage['language'])){
			this.app.endpoints.languages = this.app.endpoints.languages || {}
			this.app.endpoints.languages = JSON.parse(localStorage['language']);
			if (typeof fn === "function") fn();
		} else if (platformSdk.bridgeEnabled){
			platformSdk.nativeReq({
				fn: 'getFromCache',
				ctx: this,
				data: "languages",
				success: function(response){
					if (response != "" && response != undefined){
						response = decodeURIComponent(response);
						try {
							var response = JSON.parse(response);
						} catch (e){
							Bugsnag.notifyException(e, "GetLanguages From Cache Incorrect JSON.");
							newsapp.app.hd.initLang = false;
							newsapp.initLanguages(newsapp.domain, fn);
							return; 
						}

						newsapp.app.endpoints.languages = newsapp.app.endpoints.languages || {};
						newsapp.app.endpoints.languages = response;

						if (typeof fn === "function") fn();
					} else {
						newsapp.app.hd.initLang = false;
						newsapp.initLanguages(newsapp.domain, fn);
						return; 
					}
				}
			});	
		} 
	},
	saveLanguages: function(){
		var str = platformSdk.utils.validateStringifyJson(newsapp.app.endpoints.languages);
		try {
			PlatformBridge.putInCache('languages', str);
		} catch(e){
			if ('localStorage' in window && window['localStorage'] !== null){
				localStorage['language'] = str;
			}
		}
	},
	cacheLanguages: function(res, domain, fn){

		var that = this;

		newsapp.app.hd.topicsHash = res.viewporthash;
		newsapp.setState();
		res = res.topics;

		that.app.endpoints.languages = {};

		for (var lang in res){
			that.app.endpoints.languages[lang] = that.app.endpoints.languages[lang] || {};
			for (var i = 0, len = res[lang].length; i < len; i++){

				that.app.endpoints.languages[lang][res[lang][i].topicname] = {};
				that.app.endpoints.languages[lang][res[lang][i].topicname].nativename = res[lang][i].nativename;
				that.app.endpoints.languages[lang][res[lang][i].topicname].name = res[lang][i].topicname;

				if (res[lang][i].topicname === "topstories") that.app.endpoints.languages[lang][res[lang][i].topicname].url = domain + '/content/v1/channel/news/topic/top/items';
				else that.app.endpoints.languages[lang][res[lang][i].topicname].url = domain + '/content/v1/channel/news/topic/{##}/items'.replace(/{##}/, res[lang][i].topicname);
			}
		}

		newsapp.saveLanguages();
		
		if (newsapp.app.cache.currentLang === undefined) {
			newsapp.app.cache.currentLang = 'english';
			newsapp.setStore(newsapp.app.cache);
		}

		newsapp.renderLanguages();
		newsapp.renderCategories();

		newsapp.app.hd.initLang = true;
		newsapp.setState();

		if (typeof fn === "function") fn();
	},
	initLanguages: function(domain, fn){
		var that = this;
		if (!this.app.hd.initLang){
			if (platformSdk.helperData && platformSdk.helperData.topics) newsapp.cacheLanguages(platformSdk.helperData.topics, domain, fn);
			else {

				platformSdk.ajax({
					url: domain + '/content/v1/channel/news/topics',
					headers: [['Content-Type', 'application/json']],
					type: 'POST',
					success: function(res){
						try { res = JSON.parse(res); }
						catch(e) { 
							Bugsnag.notifyException(e, "Init Topics Server Call Incorrect JSON.");
							console.log('invalid api response'); 
						}

						newsapp.cacheLanguages(res, domain, fn);
					},
					error: function(res){

						console.log('xhr error occured on getLanguages with response', res);
						ajaxtimer = new Date().getTime() - ajaxtimer;

						if (ajaxtimer > 20000) platformSdk.events.publish('app/ajax/sendreport', {time: ajaxtimer, status: 'error', type: 'getLanguages'});

						newsapp.app.hd.initLang = false;
						newsapp.setState();
	
						if (newsapp.xhrCount != undefined) newsapp.xhrCount++;
						else newsapp.xhrCount = 1;

						if (newsapp.xhrCount > newsapp.retryCount) return false;
						
						newsapp.initLanguages(domain, fn);
						newsapp.xhrready = true;
						return false;						
					}
				});
			}
		} else {
			this.getLanguages(function(){
				if (newsapp.app.cache.currentLang === undefined) newsapp.app.cache.currentLang = "english";
				that.renderLanguages();
				that.renderCategories();

				if (typeof fn === "function") fn();
			});
		}
	},
	initApp: function(fn){
		if (newsapp.env === "dev"){
			var domain = newsapp.domain  = 'http://stagingcontent.app.hike.in';
			var subscribeDomain = newsapp.subscriptionDomain = "http://qa-content.hike.in";
		} else {
			var domain = newsapp.domain = 'http://content.app.hike.in';
			var subscribeDomain = newsapp.subscriptionDomain = "http://subscription.platform.hike.in";
		}
		
		this.app.endpoints = {
			'viewporthash': domain + '/content/v1/getviewhash/channel/news/topic/',
			'getIds': domain + '/content/v1/channel/news/topic/{##}/items/ids',  		// payload: start and end
			'fetchId': domain + '/content/v1/items',									// payload: array of item ids
			
			'topstories': domain + '/content/v1/channel/news/topic/top/items',
			'india': domain + '/content/v1/channel/news/topic/india/items',
			'world': domain + '/content/v1/channel/news/topic/world/items',
			'business': domain + '/content/v1/channel/news/topic/business/items',
			'tech': domain + '/content/v1/channel/news/topic/tech/items',
			'entertainment': domain + '/content/v1/channel/news/topic/entertainment/items',
			'lifestyle': domain + '/content/v1/channel/news/topic/lifestyle/items',
			'sports': domain + '/content/v1/channel/news/topic/sports/items',

			'subscribe': subscribeDomain + '/subscription/api/v3/microapps/subscribe.json',
			'unsubscribe': subscribeDomain + '/subscription/api/v3/microapps/unsubscribe.json'
		};

		newsapp.latency = (function(){
			var ix = 0;
			var tl = 0;

			return function(lat){
				tl = tl + lat;
				newsapp.avgLatency = tl/++ix;
			};
		})();

		this.initLanguages(domain, function(){
			this.app.mt = {
				currentItem: 1,
				currentItems: 0,
				totalItems: 100,
				currentCategory: 'topstories',
				loadIncrement: 10,
				imagesLoaded: 3
			};

			if (typeof fn === "function") fn();	
		});
	},
	initGUI: function(lang){

		if (lang === undefined) lang = languageMap.english;

		this.app.sel.getCategory.children[0].innerHTML = lang.category;
		this.app.sel.getLanguage.children[0].innerHTML = lang.lang;
		this.app.sel.fullStory.children[0].innerHTML = lang.fullStory;
	},
	init: function(){

		var ae = {};
		ae["ek"] = "micro_app";
		ae["event"] = "newsapp_init";
		ae["mapp_vs"] = newsapp.version;
		console.log("newsapp_init", ae);

		if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);

		var that = this;
		this.app = new app();

		this.getStateData(function(){

			if (platformSdk.helperData && platformSdk.helperData != ""){
				try {
					var hd = platformSdk.helperData = JSON.parse(platformSdk.helperData);
					if (hd.env) newsapp.env = hd.env;
					if (hd.ver) newsapp.version = hd.ver;
					if (hd.logActivity) newsapp.logActivity = true;
					if (hd.logImageLoad) newsapp.logImageLoad = true;
				} catch(e){
					Bugsnag.notifyException(e, "Packet HelperData Incorrect JSON.");
				}
			}

			newsapp.logActivity = true;

			if (that.app.hd.ftue === undefined && that.app.hd.ftueIntro === undefined){
				that.app.hd.ftue = true;
				that.app.hd.ftueIntro = true;
				that.app.hd.block = false;
				that.app.hd.mute = false;
				that.app.hd.imageMode = true;
			}

			that.app.hd = platformSdk.utils.extend({}, that.app.hd, platformSdk.helperData);
			that.app.hd.languageIntro = platformSdk.helperData && platformSdk.helperData.languageIntro;

			var ae = {};
			ae["ek"] = "micro_app";
			ae["event"] = "newsapp_state_data";
			ae["mapp_vs"] = newsapp.version;
			console.log("newsapp_state_data", ae);
			
			if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);

			that.initDomSelectors.call(that.app);
			that.getStore(function(){

				if (newsapp.app.hd.ftueAutoIntro && newsapp.app.hd.ftueIntro) newsapp.introAuto();
				else if (newsapp.app.hd.ftueIntro) newsapp.intro();

				if (typeof newsapp.app.cache.currentLang === "undefined"){
					console.log("resetting cache for first time");
					// platformSdk.events.publish('app/showSplash');
					newsapp.app.cache = {};

					if (platformSdk.locale && that.app.hd.respectLocale){
						switch(platformSdk.locale){
							case "hi": 
								newsapp.app.cache.currentLang = "hindi";
								break;
							case "en":
								newsapp.app.cache.currentLang = "english";
								break;
						};
					} else {
						newsapp.app.cache.currentLang = "english";	
					}
					
					newsapp.app.cache[newsapp.app.cache.currentLang] = {};
					newsapp.app.cache[newsapp.app.cache.currentLang].currentCategory = "topstories";
				}

				that.initGUI(languageMap[newsapp.app.cache.currentLang]);
				that.initOverflowMenu();
				that.bindEvents();

				var ae = {};
				ae["ek"] = "micro_app";
				ae["event"] = "newsapp_cache_init";
				ae["mapp_vs"] = newsapp.version;
				console.log("newsapp_cache_init", ae);
				
				if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);

				console.log("newsapp.app.hd.ftueAutoIntro", newsapp.app.hd.ftueAutoIntro);
				console.log("newsapp.app.hd.ftueIntro", newsapp.app.hd.ftueIntro);

				that.initApp(function(){	
					
					if (platformSdk.block === "true"){
						platformSdk.events.publish('app.state.block.show');

						var unblockSwitch = document.getElementsByClassName('primary')[0];
						unblockSwitch.addEventListener('touchend', function(ev){
							if (that.app.cache.currentLang === undefined){
								that.app.cache.currentLang = 'english';	
							} 

							document.body.className = "news " + newsapp.app.cache.currentLang;
							
							that.app.cache[that.app.cache.currentLang] = {};
							that.app.cache[that.app.cache.currentLang].currentCategory = "topstories";
							that.app.cache[that.app.cache.currentLang]["topstories"] = {};
							that.pull();

							that.app.sel.content.classList.remove('hide');
							
							platformSdk.block = "false";
							platformSdk.unblockChatThread();
							platformSdk.events.publish('app.state.block.hide');
							
							var id = "" + platformSdk.retrieveId('app.menu.om.block');
							platformSdk.updateOverflowMenu(id, {
								"title": "Block"
							});

							var url = newsapp.app.endpoints.subscribe;
							var bool = false;

							// newsapp.subscribeRoutine('block', bool, url, newsapp.subBlock);
							if (newsapp.app.hd.ftueIntro) newsapp.intro();
						});

						return false;
					}

					if (!platformSdk.utils.isEmpty(platformSdk.notifData)){
						var get_latest_story = newsapp.getLatestNews(platformSdk.notifData);
						var item = platformSdk.notifData[get_latest_story];
						
						if (item.title === "service"){
							if (item.wipeData === true) newsapp.app.cache = {};
							if (item.wipeFtue === true) {
								newsapp.app.hd = {};
								that.app.hd.ftue = true;
								that.app.hd.ftueIntro = true;
								that.app.hd.block = false;
								that.app.hd.mute = false;
								that.app.hd.imageMode = true;
								newsapp.setState();
							}

							if (item.sendLogs === true){
								platformSdk.ajax({
									url: "",
									type: 'POST',
									timeout: 30000,
									data: { cache: newsapp.app.cache, hd: newsapp.app.hd },
									success: function(res){

									},
									error: function(res){

									}
								})
							}

							PlatformBridge.deleteAllNotifData();
						} else {
							var now = new Date().getTime();
							item.ts = newsapp.timeago(now - item.published_ts);
							newsapp.prepareSplash('news', item);

							newsapp.currentNotif = item;			// saving the current notif in memory for forward/share ops.

							var ae = {};
							ae["ek"] = "active_story";
							ae["c_id"] = item.id;
							ae["source"] = item.source;
							ae["p_ts"] = item.published_ts;
							ae["cat"] = "breakingNews";
							ae["cat"] = newsapp.app.cache.currentLang + "_breakingNews";
							ae["mapp_vs"] = newsapp.version;

							if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);
						}
					}
					
					if (platformSdk.utils.isEmpty(that.app.cache) || newsapp.app.hd.ftueIntro === true) {
						platformSdk.events.publish('app/showSplash');

						that.app.cache[that.app.cache.currentLang] = {};
						that.app.cache[that.app.cache.currentLang].currentCategory = 'topstories';

						if (that.app.cache.hashmap === undefined) that.app.cache.hashmap = {};
						if (that.app.cache.hashmap[lang] === undefined) that.app.cache.hashmap[lang] = that.app.cache.hashmap[lang] || {};

						document.body.className = "news " + newsapp.app.cache.currentLang;
						
						try {
							var hd = JSON.parse(platformSdk.helperData);
							var lang = that.app.cache.currentLang;
							var cat = that.app.cache[lang].currentCategory;
							if (hd.stories.length > 0 && newsapp.app.hd.ftue){
								var data = hd.stories;

								if (that.app.cache[lang].topstories === undefined) that.app.cache[lang].topstories = {};
								if (that.app.cache[lang].hashmap.topstories === undefined) that.app.cache[lang].hashmap.topstories = {};

								that.app.cache[lang][cat]['items'] = [];
								that.app.cache[lang][cat]['hash'] = 1;
								that.app.cache[lang][cat]['mt'] = { currentItem: 1, currentItems: 0, totalItems: 100, currentCategory: '', loadIncrement: 10, imagesLoaded: 3 };

								for (var i = 0, len = data.length; i < len; i++){
									var a = data[i];
									
									a.ts = newsapp.timeago(120000);
									a.idx = (i + 1);
									a.count = 1;
									a.topicname = "topstories";
									a.away = false;
									a.active = false;
									a.imagedata = "";

									that.app.cache[lang][cat]['items'].push(a);
									that.app.cache[lang].hashmap[cat][a.id] = 1;
									that.app.cache[lang][cat].mt.currentItems += 1;
								}

								platformSdk.events.publish('app/draw', 1);
								newsapp.bootstrapped = true;
								delete newsapp.app.hd.stories;
								newsapp.setState();
							}
						} catch(e) {
							Bugsnag.notifyException(e, "Packet HelperData Incorrect JSON.");
							console.log('empty helperData');
						}

						that.pull();
						that.app.sel.content.classList.remove('hide');
					} else {
						document.body.className = "news " + newsapp.app.cache.currentLang;
						newsapp.fetchDiff();
						that.app.sel.content.classList.remove('hide');
					}

					// bot open event
					var ae = {};
					ae["ek"] = "micro_app";
					ae["event"] = "mapp_bot_open";
					ae["fld1"] = newsapp.app.cache.currentLang;
					ae["mapp_vs"] = newsapp.version;

					if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);

					if (newsapp.app.hd.languageIndicator === undefined || newsapp.app.hd.languageIndicator === true){
						var indicator = document.createElement('div');
						indicator.classList.add('load');
						newsapp.app.sel.getLanguage.appendChild(indicator);
						newsapp.app.hd.languageIndicator = true;
						newsapp.setState();
					} else {	
						if (newsapp.app.hd.categoryIndicator === undefined || newsapp.app.hd.categoryIndicator === true){
							var indicator = document.createElement('div');
							indicator.classList.add('load');
							newsapp.app.sel.getCategory.appendChild(indicator);
							newsapp.app.hd.categoryIndicator = true;
							newsapp.setState();
						}
					}

					// turning off images for 2G Networks. 
					// Also not persisting the state for next session. Hack. 

					newsapp.checkConnection(function(res){
						if (res === "2"){
							if (!newsapp.app.hd.imageMode) return;
							if (platformSdk.bridgeEnabled) PlatformBridge.showToast("Slow Network detected. Turning off images.");
							else console.log("Slow Network detected. Turning off images.");
							// newsapp.killImages = true;

							newsapp.app.hd.imageMode = true;
							platformSdk.events.publish('app.menu.om.imageModeToggle')
						}
					});

					newsapp.loadingTime = new Date().getTime() - newsapp.loadingTime;
					
					var ae = {};
					ae["ek"] = "micro_app";
					ae["event"] = "newsapp_init_time";
					ae["fld1"] = newsapp.loadingTime;
					ae["mapp_vs"] = newsapp.version;
					console.log("newsapp_init_time", ae);
					
					if (platformSdk.bridgeEnabled) platformSdk.utils.logAnalytics("true", "click", ae);

					newsapp.checkCategoryUpdate();
					newsapp.subscribeRecurringUser();
				});	
			});
		});		
	}
};

platformSdk.ready(function(){
	newsapp.loadingTime = new Date().getTime();

	Bugsnag.projectRoot = "newsapp";
	Bugsnag.metaData = { appData: JSON.stringify(platformSdk.appData)};
    Bugsnag.appVersion = newsapp.version;

    Bugsnag.beforeNotify = function () {
    	if (!newsapp.app.hd) return true;
        else if (newsapp.app.hd.logActivity) return true;
        else return false;
    };

	newsapp.init();
});

window.notifDataReceived = function(data){
	console.log('inapp push notif', data);
};
