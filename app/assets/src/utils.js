platformSdk.utils = function(window, platformSdk) {

	var platformBridge = window.PlatformBridge;

	(function(){
	  	var cache = {};
	  	this.tmpl = function tmpl(str, data){
	    	var fn = !/\W/.test(str) ? cache[str] = cache[str] || tmpl(document.getElementById(str).innerHTML) : new Function("obj", "var p=[],print=function(){p.push.apply(p,arguments);};" + "with(obj){p.push('" + str.replace(/[\r\t\n]/g, " ").split("<%").join("\t").replace(/((^|%>)[^\t]*)'/g, "$1\r").replace(/\t=(.*?)%>/g, "',$1,'").split("\t").join("');").split("%>").join("p.push('").split("\r").join("\\'") + "');}return p.join('');");
	    	return data ? fn( data ) : fn;
	  	};
	})();

	return {
		log: function(msg, caption) {
			if (platformSdk.bridgeEnabled) platformBridge.logFromJS("platform-js-sdk", msg);
			if (console) {
				// if (caption)
				// 	console.log(caption + ":");
				// console.log(msg);
			}
		},
		
		debug: function(object) {
			if (platformSdk.bridgeEnabled) platformBridge.logFromJS("platform-js-sdk", this.validateStringifyJson(object));
		},

		logAnalytics: function(isUI, type, analyticEvents) {
			analyticEvents = this.validateStringifyJson(analyticEvents);
			this.log("analytic with isui = " + isUI + " type = " + type + " analyticEvents = " + analyticEvents);
			console.log("ae: " + analyticEvents);
			if (platformSdk.bridgeEnabled) PlatformBridge.logAnalytics(isUI, type, analyticEvents);
		},

		validateStringifyJson: function(json) {
			//HACK to handle the helperdata bug. we cannot have \" or ' in the str.
			var jsonString = JSON.stringify(json);
			jsonString = jsonString.replace(/\\"/g, "&quot;");
			jsonString = jsonString.replace(/'/g, "&#39;");
			jsonString = jsonString.replace(/\\n/g," ");
			return jsonString;
		},

		merge: function(array, key) { // merges 2 arrays and de-duplicates.
			var a = array.concat();
			for (var i = 0; i < a.length; ++i) {
				for (var j = i + 1; j < a.length; ++j) {
					if (a[i][key] === a[j][key])
						a.splice(j--, 1);
				}
			}
			return a;
		},

		extend: function(out) {
		  	out = out || {};

		  	for (var i = 1; i < arguments.length; i++) {
		    	var obj = arguments[i];

		    	if (!obj) continue;

		    	for (var key in obj) {
		      		if (obj.hasOwnProperty(key)) {
		        		if (typeof obj[key] === 'object') platformSdk.utils.extend(out[key], obj[key]);
		        		else out[key] = obj[key];
		      		}
		    	}
		  	}

		  	return out;
		},

		sort: function(array, key, type) {
			return array.sort(function(a, b) {
				var x = a[key];
				var y = b[key];
				if (type === "asc") return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				else return ((x > y) ? -1 : ((x < y) ? 1 : 0));
			});
		},

		isEmpty: function(obj) {
			for (var prop in obj) {
				if (obj.hasOwnProperty(prop))
					return false;
			}

			return true;
		},

		addEventListenerList: function(list, event, fn) {
			for (var i = 0, len = list.length; i < len; i++) {
				list[i].addEventListener(event, fn, false);
			}
		},

		removeEventListenerList: function(list, event, fn) {
			for (var i = 0, len = list.length; i < len; i++) {
				list[i].removeEventListener(event, fn, false);
			}
		},

		siblings: function(n) {
			function getChildren(n, skipMe) {
				var r = [];
				var elem = null;
				for (; n; n = n.nextSibling)
					if (n.nodeType == 1 && n != skipMe)
						r.push(n);
				return r;
			}

			return getChildren(n.parentNode.firstChild, n);
		},

		scrollTo: function(elem, Y, duration, easingFunction, callback) {

			if (typeof elem == "undefined")
				elem = document.documentElement.scrollTop ? document.documentElement : document.body;
			var start = Date.now();
			var from = elem.scrollTop;

			if (from === Y) {
				if (callback) callback();
				return; /* Prevent scrolling to the Y point if already there */
			}

			function min(a, b) {
				return a < b ? a : b;
			}

			function scroll(timestamp) {

				var currentTime = Date.now(),
					time = min(1, ((currentTime - start) / duration)),
					easedT = easingFunction(time);

				elem.scrollTop = (easedT * (Y - from)) + from;

				if (time < 1) requestAnimationFrame(scroll);
				else
				if (callback) callback();
			}

			requestAnimationFrame(scroll);
		},

		easing: {
			// no easing, no acceleration
			linear: function(t) {
				return t;
			},

			// accelerating from zero velocity
			easeInQuad: function(t) {
				return t * t;
			},

			// decelerating to zero velocity
			easeOutQuad: function(t) {
				return t * (2 - t);
			},

			// acceleration until halfway, then deceleration
			easeInOutQuad: function(t) {
				return t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t;
			},

			// accelerating from zero velocity
			easeInCubic: function(t) {
				return t * t * t;
			},

			// decelerating to zero velocity
			easeOutCubic: function(t) {
				return (--t) * t * t + 1;
			},

			// acceleration until halfway, then deceleration
			easeInOutCubic: function(t) {
				return t < 0.5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1;
			},

			// accelerating from zero velocity
			easeInQuart: function(t) {
				return t * t * t * t;
			},

			// decelerating to zero velocity
			easeOutQuart: function(t) {
				return 1 - (--t) * t * t * t;
			},

			// acceleration until halfway, then deceleration
			easeInOutQuart: function(t) {
				return t < 0.5 ? 8 * t * t * t * t : 1 - 8 * (--t) * t * t * t;
			},

			// accelerating from zero velocity
			easeInQuint: function(t) {
				return t * t * t * t * t;
			},

			// decelerating to zero velocity
			easeOutQuint: function(t) {
				return 1 + (--t) * t * t * t * t;
			},

			// acceleration until halfway, then deceleration
			easeInOutQuint: function(t) {
				return t < 0.5 ? 16 * t * t * t * t * t : 1 + 16 * (--t) * t * t * t * t;
			}
		},

		getHeight: function(el) {
			var children = el.children;
			var len = children.length;
			var height = 0;

			for (var i = 0; i < len; i++) {
				height = height + parseInt(children[i].offsetHeight);
			}
			return height;
		},
		closest: function(el, tag) {
		  	tag = tag.toUpperCase();
		  	do {
		    	if (el.nodeName === tag) return el;
		  	} while (el = el.parentNode);
		  	
		  	return null;
		}
	};

}(window, window.platformSdk);
