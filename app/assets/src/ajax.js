platformSdk.ajax = function(window, platformSdk) {

	var platformBridge = window.PlatformBridge;

	var ajaxSuccess = function(xhr, callback) {
		if (callback && typeof callback === 'function')
			callback(xhr.responseText);
	};

	var ajaxError = function(xhr, callback, errorMsg) {
		if (callback && typeof callback === 'function')
			callback(xhr);
		if (errorMsg)
			platformBridge.showToast(errorMsg);
	};

	var checkConnection = function(fn){
		platformSdk.nativeReq({
			fn: 'checkConnection',
			ctx: this,
			data: "",
			success: function(response){
				if (response != "-1" && response != "0") {
					if (typeof fn === "function") fn(response);
				} else platformSdk.events.publish('app/offline');
			}
		});
	};

	var fire = function(obj, conn){
		var url = obj.url,
			headers = obj.headers,
			data = obj.data,
			errorMsg = obj.errorMessage,
			callbackSucess = obj.success,
			callbackFailure = obj.error,
			type = obj.type.toUpperCase();

		var xhr = new XMLHttpRequest();
		var xmlHttpTimeout;

		var ajaxTimeout = function(){
			xhr.abort();
			platformSdk.events.publish('app/ajax/timeout');
		};

		platformSdk.utils.log("ajax call started on " + url);
		if (xhr) {
			xhr.onreadystatechange = function() {
				if (4 == xhr.readyState && 200 == xhr.status) {
					clearTimeout(xmlHttpTimeout); 
					if (platformSdk.helperData && platformSdk.helperData.debug)
						platformSdk.logger.endMarker('xhrCall');
					ajaxSuccess(xhr, callbackSucess);
					platformSdk.events.publish('app/ajax/success');
				}
				if (4 == xhr.readyState && 200 != xhr.status) {
					clearTimeout(xmlHttpTimeout); 
					if (platformSdk.helperData && platformSdk.helperData.debug)
						platformSdk.logger.endMarker('xhrCall');
					ajaxError(xhr, callbackFailure, errorMsg);
					platformSdk.events.publish('app/ajax/fail');
				}
			};

			var datatype = Object.prototype.toString.call(data);
			if (datatype === '[object Object]')
				data = platformSdk.utils.validateStringifyJson(data);

			xhr.open(type, url, true);
			if (headers) {
				for (var i = 0; i < headers.length; i++) {
					xhr.setRequestHeader(headers[i][0], headers[i][1]);
				}
			}

			if (platformSdk.helperData && platformSdk.helperData.debug)
				platformSdk.logger.setMarker('xhrCall');

			xhr.send(data);
			if (conn && conn === "2") obj.timeout = obj.timeout * 1.5; 
			if (obj.timeout) xmlHttpTimeout = setTimeout(ajaxTimeout, obj.timeout);
		}
	}

	return function(obj) {

		// if (platformSdk.bridgeEnabled) {
		// 	checkConnection(function(conn){
		// 		fire(obj, conn);
		// 	});
		// } else fire(obj);

		fire(obj);
		
	};

}(window, window.platformSdk);
