platformSdk.logger = function(window, platformSdk) {

	"use strict";

	var platformBridge = window.PlatformBridge;

	var markers = {};

	var latencyData = {
		html: {}
	};

	var drawDebugInfoOverlay = function(name, dataObj) {
		var debugInfoOverlay = document.getElementById("debug-info-overlay");

		if (debugInfoOverlay) {
			debugInfoOverlay.remove();
		}

		setTimeout(function() {
			var htmlStr = name;
			var body = document.body;
			var listStr = '<ul>';
			var link = document.getElementsByTagName('link');
			var basePath = link[0].getAttribute('href').split('assets')[0];
			for (var key in dataObj) {
				listStr += '<li><b>' + key + '</b></li>';
				var keyData = dataObj[key];

				for (var key in keyData) {
					listStr += '<li>' + key + ' : ' + keyData[key] + '</li>';
				}
			}
			listStr += '</ul>';
			htmlStr = listStr + '<span class="icon-close tappingEffect" id="close-icon"><img width="14" src="' + basePath + 'assets/images/cross.png"></span>';

			var debugInfoOverlayDiv = document.createElement("div");
			debugInfoOverlayDiv.setAttribute('id', "debug-info-overlay");
			debugInfoOverlayDiv.innerHTML = htmlStr;

			body.appendChild(debugInfoOverlayDiv);

			//var closeIconDiv = document.getElementById('debug-info-overlay');
			var closeIcon = debugInfoOverlayDiv.getElementsByClassName('icon-close')[0];
			closeIcon.addEventListener('click', function() {
				debugInfoOverlayDiv.remove();
			});

		}, 15);
	};

	return {
		logLoadTimeInfo: function() {
			setTimeout(function() {
				var timingAPI;
				if (!platformSdk.helperData.debug)
					return;

				if (window.performance) {
					timingAPI = performance.timing;
				} else {
					platformSdk.utils.log("timing API not supported by the webView");
					return;
				}
				latencyData.html.networkLatency = timingAPI.responseEnd - timingAPI.fetchStart;
				latencyData.html.domReadiness = timingAPI.loadEventEnd - timingAPI.responseEnd;

				if (platformSdk.time) {
					// latencyData.native = platformSdk.time;
				}

				drawDebugInfoOverlay('DOM load', latencyData);

				platformSdk.utils.log(latencyData, 'latencyData');

			}, 100);
		},
		setMarker: function(name) {
			if (window.performance)
				window.performance.mark(name + "_marker_start");
		},
		endMarker: function(name, clearFlag) {
			if (window.performance) {
				window.performance.mark(name + "_marker_end");
				this.measureMarker(name, clearFlag);
			}
		},
		measureMarker: function(name, clearFlag) {
			var measureName = name + '_measure';
			if (!window.performance) return;

			window.performance.measure(measureName, name + '_marker_start', name + '_marker_end');
			var measures = window.performance.getEntriesByName(name + '_measure');


			platformSdk.utils.log('name: ' + measures[0].name + ', duration: ' + measures[0].duration);

			this.clearMarker(name);
			this.clearMeasure(name);

			drawDebugInfoOverlay(name, measures[0]);
		},
		clearMarker: function(name) {
			if (window.performance) {
				window.performance.clearMarks(name + "_marker_start");
				window.performance.clearMarks(name + "_marker_end");
			}
		},
		clearMeasure: function(name) {
			if (window.performance) {
				window.performance.clearMeasures(name + "_measure");
			}
		},
		clearAllMarker: function(name) {
			if (window.performance) {
				window.performance.clearMarks();
			}
		}
	};

}(window, window.platformSdk);