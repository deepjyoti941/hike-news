(function(window, platformSdk) {
	var callbacks = {};
	var eventsObject = {};

	function getNewId() {
		var cbId = Math.round(Math.random() * 999999999);
		while (cbId in callbacks) {
			cbId = Math.round(Math.random() * 999999999);
		}
		return cbId;
	}

	window.callbackFromNative = function(id, params) {

		var args, cbItem = callbacks[id];
		if (cbItem && typeof(cbItem.callback) === 'function') {
			cbItem.callback.call(cbItem.context, params);
		}

		delete callbacks[id];
	};

	platformSdk.nativeReq = function(param) {

		var callBackId = "" + getNewId();
		
		callbacks[callBackId] = {
			context: param.ctx,
			callback: param.success
		};

		if (platformSdk.bridgeEnabled) {
			if (param.data === "" || param.data === undefined || param.data === null) PlatformBridge[param.fn](callBackId);
			else PlatformBridge[param.fn](callBackId, param.data);
		}
	};

	platformSdk.setOverflowMenu = function(omList) {
		for (var i = 0; i < omList.length; i++){
			var omItem = omList[i];
			var eventId = getNewId();
			callbacks[eventId] = omItem;
			omItem.id = eventId;
		}

		omListObject = omList;
		
		if (platformSdk.bridgeEnabled) PlatformBridge.replaceOverflowMenu(platformSdk.utils.validateStringifyJson(omList));
	};

	platformSdk.onMenuItemClicked = function(id) {
		platformSdk.events.publish(callbacks[id].eventName, id);
	};

	platformSdk.updateOverflowMenu = function(id, c){
		var obj = callbacks[id];
		for (var key in c){
			obj[key] = c[key];
		}

		if (platformSdk.bridgeEnabled) PlatformBridge.updateOverflowMenu(id, platformSdk.utils.validateStringifyJson(obj));
	};

	platformSdk.retrieveId = function(eventname){
		for (var i = 0; i < omListObject.length; i++){
			var omItem = omListObject[i];
			if (omItem.eventName === eventname) return omItem.id;
		}
	};

})(window, window.platformSdk);

