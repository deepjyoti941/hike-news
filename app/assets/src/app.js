window.platformSdk = function(window, undefined) {
	"use strict";

	//classlist hack for android 2.3 and below
	if (!("classList" in document.documentElement) && Object.defineProperty && typeof HTMLElement !== "undefined") {
		Object.defineProperty(HTMLElement.prototype, "classList", {
			get: function() {
				function t(t) {
					return function(n) {
						var r = e.className.split(/\s+/),
							i = r.indexOf(n);
						t(r, i, n);
						e.className = r.join(" ");
					};
				}
				var e = this;
				var n = {
					add: t(function(e, t, n) {
						~t || e.push(n);
					}),
					remove: t(function(e, t) {
						~t && e.splice(t, 1);
					}),
					toggle: t(function(e, t, n) {
						~t ? e.splice(t, 1) : e.push(n);
					}),
					contains: function(t) {
						return !!~e.className.split(/\s+/).indexOf(t);
					},
					item: function(t) {
						return e.className.split(/\s+/)[t] || null;
					}
				};
				Object.defineProperty(n, "length", {
					get: function() {
						return e.className.split(/\s+/).length;
					}
				});
				return n;
			}
		});
	}

	var platformBridge = window.PlatformBridge;
	var fireAppInit = function() {
		var cardHeight = document.body.offsetHeight;
		if (platformBridge) platformSdk.ui.onLoadFinished(cardHeight + "");

		setTimeout(function() {
			cardHeight = document.body.offsetHeight;

			if (Math.abs(window.innerHeight - cardHeight) > 5)
				platformSdk.ui.resize(cardHeight);
			if (platformBridge) platformSdk.events.publish('onnativeready');
			else platformSdk.events.publish('webview/data/loaded');
		}, 100);
	};

	// if (platformBridge) platformBridge.setDebuggableEnabled(true);
	window.onload = fireAppInit;

	var setData = function(msisdn, helperData, isSent, uid, appVersion) {
		// platformSdk.utils.log("inSetData");
		var appData = {
			msisdn: msisdn,
			isSent: isSent,
			uid: uid,
			appVersion: appVersion
		};

		appData.helperData = JSON.parse(helperData);
		setAppData(appData);
	};

	var appInitialized = false;
	var setAppData = function(appData) {

		// console.log(appData);
		appData = decodeURIComponent(appData);

		if (appInitialized) return;
		else appInitialized = true;

		if(typeof appData === 'string'){
			appData = JSON.parse(appData);
		}

		if(appData.hd){
			appData.helperData = appData.hd;
			delete appData.hd;
		}

		// platformSdk.utils.log("init");

		if (!appData.msisdn) {
			// platformSdk.utils.log("msisdn is null");
		} else {
			// platformSdk.utils.log("msisdn: " + appData.msisdn);
			platformSdk.appData = appData;

			for (var key in appData) {
				platformSdk[key] = appData[key];
			}

			if (appData.helperData) {
				if (appData.helperData.debug) {
					platformSdk.logger.logLoadTimeInfo();
					platformBridge.setDebuggableEnabled(true);
				}
			} else platformSdk.helperData = {};
		}

		platformSdk.events.publish('webview/data/loaded');
	}

	window.setData = setData;
	window.onResume = function(){
		platformSdk.events.publish('app/onresume');
	};
	window.onPause = function(){
		platformSdk.events.publish('app/onbeforeunload');
	};
	window.init = setAppData;

	return {
		VERSION: '0.0.1',
		card: '',
		msisdn: null,
		bridgeEnabled: false,

		ready: function(fn){
			var that = this;
			var start = platformSdk.events.subscribe('webview/data/loaded', function(){
				that.bridgeEnabled = that.checkBridge();
				if (typeof fn === "function") fn();	
				start.remove();
			});
		},
		checkBridge: function(){
			return typeof PlatformBridge === "undefined" ? false : true;
		},
		blockChatThread: function() {
			platformBridge.blockChatThread("true");
		},
		unblockChatThread: function() {
			platformBridge.blockChatThread("false");
		},
		deleteMessage: function() {
			platformBridge.deleteMessage();
		},
		updateMetadata: function(data, flag) {
			platformBridge.updateMetadata(platformSdk.utils.validateStringifyJson(data), flag);
		},
		openFullPage: function(title, href) {
			platformBridge.openFullPage(title, href);
		},
		muteChatThread: function() {
			platformBridge.muteChatThread();
		},
		deleteAlarm: function() {
			platformBridge.deleteAlarm();
		},
		updateHelperData: function(data) {
			platformBridge.updateHelperData(platformSdk.utils.validateStringifyJson(data));
		},
		getBlob: function(){
			var obj = platformBridge.getLargeDataFromCache();
		},
		setBlob: function(obj){
			var str = platformSdk.utils.validateStringifyJson(obj);
			platformBridge.putLargeDataInCache(str);
		},
		setAlarm: function(alarmData, nextPollIt) {
			if (typeof alarmData !== 'string')
				alarmData = platformSdk.utils.validateStringifyJson(alarmData);

			platformBridge.setAlarm(alarmData, nextPollIt);
		}
	};
}(window);