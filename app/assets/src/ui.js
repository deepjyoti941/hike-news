platformSdk.ui = function(window, platformSdk) {

	var platformBridge = window.PlatformBridge;

	var shareMessage;
	var captionText;

	platformSdk.events.subscribe('refresh/startAnimation/', function(ele) {
		ele.classList.add('play');
	});

	platformSdk.events.subscribe('refresh/stopAnimation/', function(ele) {
		ele.classList.remove('play');
	});

	if (!platformSdk.checkBridge) return false;
	return {
		onLoadFinished: function(height) {
			platformBridge.onLoadFinished(height + "");
		},
		resize: function(height) {
			height = height || document.body.offsetHeight;
			platformBridge.onResize(height + "");
		},
		showToast: function(msg) {
			platformBridge.showToast(msg);
		},
		shareCard: function(e) {
			e.preventDefault();
			e.stopPropagation();

			platformSdk.utils.log("share calling");

			if (platformSdk.helperData != null && platformSdk.helperData.share_text) {
				shareMessage = platformSdk.helperData.share_text;
			} else {
				//shareMessage = "World Cup 2015 Live scores only on hike!";
				shareMessage = "hike up your life only on hike!";
			}
			if (platformSdk.helperData != null && platformSdk.helperData.caption_text) {
				captionText = platformSdk.helperData.caption_text;
			} else {
				captionText = "";
			}

			platformBridge.share(shareMessage, captionText);
			platformSdk.utils.log("share called");

			return false;
		},
		forwardCard: function(e) {
			e.preventDefault();
			e.stopPropagation();
			//addRippleEffect(e);

			platformSdk.utils.log("forward calling");
			platformBridge.forwardToChat(platformSdk.forwardCardData);
			platformSdk.utils.log("forward callied  with json=" + platformSdk.forwardCardData);

			return false;
		}
	};
}(window, window.platformSdk);
