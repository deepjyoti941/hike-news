/*
 * Minimal classList shim for IE 9
 * By Devon Govett
 * MIT LICENSE
 */
if(!("classList"in document.documentElement)&&Object.defineProperty&&typeof HTMLElement!=="undefined"){Object.defineProperty(HTMLElement.prototype,"classList",{get:function(){function t(t){return function(n){var r=e.className.split(/\s+/),i=r.indexOf(n);t(r,i,n);e.className=r.join(" ")}}var e=this;var n={add:t(function(e,t,n){~t||e.push(n)}),remove:t(function(e,t){~t&&e.splice(t,1)}),toggle:t(function(e,t,n){~t?e.splice(t,1):e.push(n)}),contains:function(t){return!!~e.className.split(/\s+/).indexOf(t)},item:function(t){return e.className.split(/\s+/)[t]||null}};Object.defineProperty(n,"length",{get:function(){return e.className.split(/\s+/).length}});return n}})} 

// Simple JavaScript Templating
// John Resig - http://ejohn.org/ - MIT Licensed
(function(){
  	var cache = {};
  	this.tmpl = function tmpl(str, data){
    	var fn = !/\W/.test(str) ? cache[str] = cache[str] || tmpl(document.getElementById(str).innerHTML) : new Function("obj", "var p=[],print=function(){p.push.apply(p,arguments);};" + "with(obj){p.push('" + str.replace(/[\r\t\n]/g, " ").split("<%").join("\t").replace(/((^|%>)[^\t]*)'/g, "$1\r").replace(/\t=(.*?)%>/g, "',$1,'").split("\t").join("');").split("%>").join("p.push('").split("\r").join("\\'") + "');}return p.join('');");
    	return data ? fn( data ) : fn;
  	};
})();

var events = (function(){
	var topics = {};
	var hOP = topics.hasOwnProperty;

	return {
    	subscribe: function(topic, listener) {
      		if(!hOP.call(topics, topic)) topics[topic] = [];
      		var index = topics[topic].push(listener) -1;
  			return {
    			remove: function() {
      				delete topics[topic][index];
    			}
  			};
    	},
    	publish: function(topic, info) {
			if(!hOP.call(topics, topic)) return;
      		topics[topic].forEach(function(item) {
      			item(info != undefined ? info : {});
      		});
    	}
  	};
})();

var h = {
	updateHelperData: function(helperData){
	    //HACK to handle the helperdata bug. we cannot have \" or ' in the str.
	    var hdstr = JSON.stringify(helperData);
      	hdstr = hdstr.replace(/\\"/g, "&quot;");
      	hdstr = hdstr.replace(/'/g, "&#39;");
	    PlatformBridge.updateHelperData(hdstr);
	},
	merge: function(array, key) {		// merges 2 arrays and de-duplicates.
	    var a = array.concat();
	    for(var i=0; i<a.length; ++i) {
	        for(var j=i+1; j<a.length; ++j) {
	            if(a[i][key] === a[j][key])
	                a.splice(j--, 1);
	        }
	    }
	    return a;
	},
	sort: function(array, key, type) {
	    return array.sort(function(a, b) {
	        var x = a[key]; var y = b[key];
	        if (type === "asc") return ((x < y) ? -1 : ((x > y) ? 1 : 0));
	        else return ((x > y) ? -1 : ((x < y) ? 1 : 0));
	    });
	},
	isEmpty: function(obj) {
	    for(var prop in obj) {
	        if(obj.hasOwnProperty(prop))
	            return false;
	    }

	    return true;
	},
	addEventListenerList: function(list, event, fn){
	    for (var i = 0, len = list.length; i < len; i++) {
	        list[i].addEventListener(event, fn, false);
	    }
	},
	removeEventListenerList: function(list, event, fn){
	    for (var i = 0, len = list.length; i < len; i++) {
	        list[i].removeEventListener(event, fn, false);
	    }
	},
	siblings: function(n){
		function getChildren(n, skipMe){
		    var r = [];
		    var elem = null;
		    for ( ; n; n = n.nextSibling ) 
		       if ( n.nodeType == 1 && n != skipMe)
		          r.push( n );        
		    return r;
		};

		return getChildren(n.parentNode.firstChild, n);		
	},
	scrollTo: function(elem, Y, duration, easingFunction, callback) {
    	
    	if (typeof elem == "undefined") var	elem = document.documentElement.scrollTop?document.documentElement:document.body;
	    var start = Date.now();
	    var from = elem.scrollTop;
	 
	    if(from === Y) {
	        if (callback) callback();
	        return; /* Prevent scrolling to the Y point if already there */
	    }
	 
	    function min(a,b) {
	    	return a<b?a:b;
	    }
	 
	    function scroll(timestamp) {
	 
	        var currentTime = Date.now(),
	            time = min(1, ((currentTime - start) / duration)),
	            easedT = easingFunction(time);
	 
	        elem.scrollTop = (easedT * (Y - from)) + from;
	 
	        if(time < 1) requestAnimationFrame(scroll);
	        else
	            if(callback) callback();
	    }
	 
	    requestAnimationFrame(scroll)
	},
	easing: {
	  // no easing, no acceleration
	  linear: function (t) { return t },
	  // accelerating from zero velocity
	  easeInQuad: function (t) { return t*t },
	  // decelerating to zero velocity
	  easeOutQuad: function (t) { return t*(2-t) },
	  // acceleration until halfway, then deceleration
	  easeInOutQuad: function (t) { return t<.5 ? 2*t*t : -1+(4-2*t)*t },
	  // accelerating from zero velocity 
	  easeInCubic: function (t) { return t*t*t },
	  // decelerating to zero velocity 
	  easeOutCubic: function (t) { return (--t)*t*t+1 },
	  // acceleration until halfway, then deceleration 
	  easeInOutCubic: function (t) { return t<.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1 },
	  // accelerating from zero velocity 
	  easeInQuart: function (t) { return t*t*t*t },
	  // decelerating to zero velocity 
	  easeOutQuart: function (t) { return 1-(--t)*t*t*t },
	  // acceleration until halfway, then deceleration
	  easeInOutQuart: function (t) { return t<.5 ? 8*t*t*t*t : 1-8*(--t)*t*t*t },
	  // accelerating from zero velocity
	  easeInQuint: function (t) { return t*t*t*t*t },
	  // decelerating to zero velocity
	  easeOutQuint: function (t) { return 1+(--t)*t*t*t*t },
	  // acceleration until halfway, then deceleration 
	  easeInOutQuint: function (t) { return t<.5 ? 16*t*t*t*t*t : 1+16*(--t)*t*t*t*t }
	},
	getHeight: function(el){
		var children = el.children;
		var len = children.length;
		var height = 0;

		for (var i = 0; i < len; i++){
			height = height + parseInt(children[i].offsetHeight);
		}
		return height;
	}
};

var platform = platform = {
	refreshClick: false,
	helperData: {},
	msisdn: '',
	config: {},
	response: '',
	checkBridge: function(){
		if (typeof PlatformBridge === 'object') return true;
		else return false;
	},
	ajax: function(x){
		var xhr = new XMLHttpRequest();
      	if (xhr && navigator.onLine) {
          	xhr.onreadystatechange = function() {
          		switch(xhr.readyState){
          			case 2:
          			case 3:
          				if (xhr.status != 200){
          					if (typeof x.error === "function") x.error(xhr.statusText, xhr.status);
          					if (platform.checkBridge()) PlatformBridge.showToast("Something bad happened. Rebooting News.");
          				}
          			break;
          			case 4:
          				if (xhr.status == 200){
          					if (typeof x.success === 'function') 
          						x.success(xhr.responseText);		
          				} else {
          					if (typeof x.error === "function") x.error(xhr.statusText, xhr.status);	
          					if (platform.checkBridge()) PlatformBridge.showToast("Something bad happened.");
          				}
          		};
          	}

          	var datatype = Object.prototype.toString.call(x.data);
          	if (datatype === '[object Object]') x.data = JSON.stringify(x.data);
			
          	xhr.open(x.type, x.url, true);
          	if (x.headers){
          		for (var i = 0; i < x.headers.length; i++){
          			xhr.setRequestHeader(x.headers[i][0], x.headers[i][1]);
          		}
          	}
          	xhr.send(x.data);
      	} else {
      		if (typeof x.error === "function"){
      			x.error("No Internet Connection", -1);
      			if (platform.checkBridge()) {
      				PlatformBridge.showToast("No Internet Conection.");
      			}
      		}
      	}
	},
    ajaxCall: function(type, url, callback, data, headers, errorcallback) {
      	var xhr = new XMLHttpRequest();

      	if (xhr) {
          	xhr.onreadystatechange = function() {
           		if ( 4 == xhr.readyState && 200 == xhr.status ) if (typeof callback === 'function') callback(xhr.responseText);
           		else if (typeof errorcallback === "function") errorcallback(xhr.responseText, xhr.status);
          	}

          	var datatype = Object.prototype.toString.call(data);
          	if (datatype === '[object Object]') data = JSON.stringify(data);
			
          	xhr.open(type, url, true);
          	if (headers){
          		for (var i = 0; i < headers.length; i++){
          			xhr.setRequestHeader(headers[i][0], headers[i][1]);
          		}
          	}
          	xhr.send(data);
      	}
	},

	log: function (message) {
		// PlatformBridge.logFromJS("cricket-fever", message);
		console.log(message);
	},

	debug: function(object){
		PlatformBridge.logFromJS("cricket-fever", JSON.stringify(object));
	},

	logAnalytics: function(isUI, type, analyticEvents){
		analyticEvents = JSON.stringify(analyticEvents);
		platform.log("analytic with isui = "+ isUI + " type = "+ type + " analyticEvents = "+ analyticEvents);
	}

};

function onStop (func) {
	func();
}

function setData(msisdn, helperData){
	platform.log("inSetData");

	if (!msisdn){
		platform.log("msisdn is null");
	} else {
		if (helperData != null && helperData !=''){
			platform.helperData = JSON.parse(helperData);
			platform.log("helperData: "+platform.helperData);
			if(typeof platform.helperData.debug == "undefined" ) {
				platform.helperData.debug = true;
			}
		}
	}

	if (platform.checkBridge()) PlatformBridge.setDebuggableEnabled(true);
	events.publish('app/onnativeready');
}

function pullNavigator(){
	platform.debug(navigator);
};

function onResume(){
	platform.log("in on resume");
	events.publish('app/onresume');
}

function onPause(){
	platform.log("in on pause");
	events.publish('app/onbeforeunload');
}

window.onerror = function(error, url, line){
	platform.log('error: ' + error + url + line);
};

window.onload = function(){

};

var fireAppInit = function(){
	if (fireappload != undefined){
		events.publish('/fire/page/load/');
	} else {
		setTimeout(function(){
			fireAppInit();
		}, 10);
	}
};

events.subscribe('/platform/app.init/', function(){
	
	// checks if all present images have loaded before 
	// calculating the onLoadFinished height for native

	var images = document.querySelectorAll('img');
	var num_of_images = images.length;
	var img_loaded = 0;

	platform.log(num_of_images);
	platform.log(img_loaded);

	var imgload = events.subscribe('/img/loaded/', function(){
		img_loaded++;

		platform.log(img_loaded);
		if (img_loaded === num_of_images) {
			fireAppInit();
			imgload.remove();
		}
	});

	if (num_of_images > 0){
		for (var i = 0; i < num_of_images; i++){
			platform.log(images[i]);
			if (images[i].complete){
				events.publish('/img/loaded/');
				platform.log('image already loaded: ', img_loaded);
			} else {
				images[i].addEventListener('load', function(ev){
					platform.log('images onload event');
					events.publish('/img/loaded/');
				});
			}
		}	
	} else {
		fireAppInit();
		imgload.remove();
	}
});
