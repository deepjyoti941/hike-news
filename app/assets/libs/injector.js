var removeShare = function(arr){
	for (var i = 0; i < arr.length; i++){
		var selectors = arr[i];

		if (selectors.length > 0){
			for (var j = 0; j < selectors.length; j++){
				var el = selectors[j];
				el.parentNode.removeChild(el);
			}	
		}
	}
};



	if (typeof jQuery === "function"){
		jQuery.expr[':'].regex = function(elem, index, match) {
		    var matchParams = match[3].split(','),
		        validLabels = /^(data|css):/,
		        attr = {
		            method: matchParams[0].match(validLabels) ? 
		                        matchParams[0].split(':')[0] : 'attr',
		            property: matchParams.shift().replace(validLabels,'')
		        },
		        regexFlags = 'ig',
		        regex = new RegExp(matchParams.join('').replace(/^s+|s+$/g,''), regexFlags);
		    return regex.test(jQuery(elem)[attr.method](attr.property));
		};

		var arr = [];

		arr.push(jQuery('div:regex(class,social)'));
		arr.push(jQuery('ul:regex(class,social)'));
		arr.push(jQuery('div:regex(class,share)'));
		arr.push(jQuery('ul:regex(class,share)'));
		arr.push(jQuery('div:regex(id,social)'));
		arr.push(jQuery('div:regex(id,share)'));
		arr.push(jQuery('ul:regex(id,social)'));
		arr.push(jQuery('ul:regex(id,share)'));

		removeShare(arr);
	} else {
		var arr = [];
		arr.push(document.querySelectorAll('div[class^="social"]'));
		arr.push(document.querySelectorAll('div[id^="social"]'));
		arr.push(document.querySelectorAll('ul[class^="social"]'));
		arr.push(document.querySelectorAll('ul[id^="social"]'));
		arr.push(document.querySelectorAll('div[class^="share"]'));
		arr.push(document.querySelectorAll('div[id^="share"]'));
		arr.push(document.querySelectorAll('ul[class^="share"]'));
		arr.push(document.querySelectorAll('ul[id^="share"]'));
		
		removeShare(arr);
	}
