(function () {
    'use strict';

    var BUILD_PATH = 'build',
        STAGING_PATH = BUILD_PATH + '/staging/newsapp',
        PROD_PATH = BUILD_PATH + '/prod/newsapp',
        DEV_PATH = BUILD_PATH + '/dev/newsapp',
        IMAGES_PATH = '/assets/images',
        FONT_PATH = '/assets/font',
        CSS_PATH = '/assets/css',
        JS_PATH = '/assets/js',
        gulp = require('gulp'),
        del = require('del'),
        fs = require('fs'),
        stripLine = require('gulp-strip-line'),
        replace = require('gulp-replace'),
        sass = require('gulp-sass'),
        uglify = require('gulp-uglify'),
        minifyCss = require('gulp-minify-css'),
        webpack = require('gulp-webpack'),

        uglifyStagingParams = {
            mangle: false,
            output: {
                beautify: true
            },
            compress: {
                drop_console: false
            }
        },

        uglifyProdParams = {
            mangle: false,
            output: {
                beautify: true
            },
            compress: {
                drop_console: true
            }
        };

    gulp.task('clean', function (cb) {
        del(BUILD_PATH, cb);
    });

    gulp.task('prodCopy', ['jsProd'], function () {
        var psdk = fs.readFileSync(PROD_PATH + '/platformSdk.min.js', 'utf8');
        var app = fs.readFileSync(PROD_PATH + '/app.js', 'utf8');
        var newsinit = fs.readFileSync(PROD_PATH + '/newsinit.js', 'utf8');

        gulp.src('newscard.html')
            .pipe(gulp.dest(PROD_PATH));

        return gulp.src('newsapp.html')
            .pipe(replace('INLINE_SCRIPT_PSDK', psdk))
            .pipe(replace('INLINE_SCRIPT_APP', app))
            .pipe(replace('INLINE_SCRIPT_NEWSINIT', newsinit))
            .pipe(gulp.dest(PROD_PATH));
    });

    gulp.task('copyCss', ['clean'], function () {
        return gulp.src('assets/css/*')
            .pipe(gulp.dest(PROD_PATH + CSS_PATH));
    });

    gulp.task('copyFonts', ['clean'], function () {
        return gulp.src('assets/font/**')
            .pipe(gulp.dest(PROD_PATH + FONT_PATH));
    });

    gulp.task('copyImages', ['clean'], function () {
        return gulp.src('assets/images/**/*')
            .pipe(gulp.dest(PROD_PATH + IMAGES_PATH));
    });

    gulp.task('jsProd', ['clean'], function () {
        gulp.src('assets/js/app.js')
            .pipe(uglify(uglifyProdParams))
            .pipe(gulp.dest(PROD_PATH));

        gulp.src('assets/js/newsinit.js')
            .pipe(uglify(uglifyProdParams))
            .pipe(gulp.dest(PROD_PATH));

        gulp.src('assets/js/bugsnag-2.min.js')
            .pipe(gulp.dest(PROD_PATH + JS_PATH));

        return gulp.src('assets/js/platformSdk.min.js')
            .pipe(gulp.dest(PROD_PATH));
    });

    gulp.task('buildCleanUp', ['prodCopy'], function () {
        var jsBundlePath = '/bundle.js';

        del([PROD_PATH + '/platformSdk.min.js', PROD_PATH + '/app.js', PROD_PATH + '/newsinit.js']);
    });

    gulp.task('watch', function () {
        gulp.watch([
            'sass/*',
            'templates/**/*',
            'js/**/*'
        ], ['default']);


        return gulp.watch('sass/*', ['sass']);
    });

    gulp.task('default', ['copyFonts', 'copyCss', 'copyImages', 'prodCopy', 'buildCleanUp']);
})();